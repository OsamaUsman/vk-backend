﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VirtualKitchenCore.Migrations
{
    public partial class today : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "addOns",
                table: "Carts",
                type: "nvarchar(1500)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "variants",
                table: "Carts",
                type: "nvarchar(1500)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "addOns",
                table: "Carts");

            migrationBuilder.DropColumn(
                name: "variants",
                table: "Carts");
        }
    }
}
