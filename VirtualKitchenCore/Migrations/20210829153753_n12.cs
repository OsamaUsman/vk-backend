﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VirtualKitchenCore.Migrations
{
    public partial class n12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "type",
                table: "OrderDetails",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "type",
                table: "Carts",
                type: "nvarchar(150)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "type",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "type",
                table: "Carts");
        }
    }
}
