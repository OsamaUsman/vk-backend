﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VirtualKitchenCore.Migrations
{
    public partial class nsa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "addOnsIds",
                table: "OrderDetails",
                type: "nvarchar(1550)",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "variantId",
                table: "OrderDetails",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "addOnsIds",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "variantId",
                table: "OrderDetails");
        }
    }
}
