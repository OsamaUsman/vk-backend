﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VirtualKitchenCore.Migrations
{
    public partial class moreadditions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "firstName",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "lastName",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "mobileNo",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "shopName",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "suspendedTill",
                table: "Vendors");

            migrationBuilder.RenameColumn(
                name: "tradeLicense",
                table: "Vendors",
                newName: "contactNo");

            migrationBuilder.AlterColumn<string>(
                name: "email",
                table: "Vendors",
                type: "nvarchar(150)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "aboutUs",
                table: "Vendors",
                type: "nvarchar(1000)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "cover",
                table: "Vendors",
                type: "nvarchar(500)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "landlineNo",
                table: "Vendors",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "latitude",
                table: "Vendors",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "licenseSource",
                table: "Vendors",
                type: "nvarchar(250)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "logo",
                table: "Vendors",
                type: "nvarchar(500)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "longitude",
                table: "Vendors",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "mapAddress",
                table: "Vendors",
                type: "nvarchar(500)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "restaurantName",
                table: "Vendors",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "specialties",
                table: "Vendors",
                type: "nvarchar(1000)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "tradeLicenseExpiry",
                table: "Vendors",
                type: "nvarchar(250)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "tradeLicenseNo",
                table: "Vendors",
                type: "nvarchar(250)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "url",
                table: "Vendors",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "vendorId",
                table: "Products",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_vendorId",
                table: "Products",
                column: "vendorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Vendors_vendorId",
                table: "Products",
                column: "vendorId",
                principalTable: "Vendors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Vendors_vendorId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_vendorId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "aboutUs",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "cover",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "landlineNo",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "latitude",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "licenseSource",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "logo",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "longitude",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "mapAddress",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "restaurantName",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "specialties",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "tradeLicenseExpiry",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "tradeLicenseNo",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "url",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "vendorId",
                table: "Products");

            migrationBuilder.RenameColumn(
                name: "contactNo",
                table: "Vendors",
                newName: "tradeLicense");

            migrationBuilder.AlterColumn<string>(
                name: "email",
                table: "Vendors",
                type: "nvarchar(50)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(150)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "firstName",
                table: "Vendors",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "lastName",
                table: "Vendors",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "mobileNo",
                table: "Vendors",
                type: "nvarchar(100)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "shopName",
                table: "Vendors",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "suspendedTill",
                table: "Vendors",
                type: "datetime2",
                nullable: true);
        }
    }
}
