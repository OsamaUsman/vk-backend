﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VirtualKitchenCore.Migrations
{
    public partial class newAdditons : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "keywords",
                table: "Products",
                type: "nvarchar(1550)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Chats",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    userId = table.Column<long>(type: "bigint", nullable: false),
                    orderId = table.Column<long>(type: "bigint", nullable: false),
                    vendorId = table.Column<long>(type: "bigint", nullable: false),
                    message = table.Column<string>(type: "nvarchar(3000)", nullable: true),
                    sentBy = table.Column<bool>(type: "bit", nullable: false),
                    type = table.Column<bool>(type: "bit", nullable: false),
                    date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    status = table.Column<string>(type: "nvarchar(150)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chats", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Chats_Orders_orderId",
                        column: x => x.orderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Chats_Users_userId",
                        column: x => x.userId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Chats_Vendors_vendorId",
                        column: x => x.vendorId,
                        principalTable: "Vendors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChatThreats",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    userId = table.Column<long>(type: "bigint", nullable: false),
                    orderId = table.Column<long>(type: "bigint", nullable: false),
                    vendorId = table.Column<long>(type: "bigint", nullable: false),
                    date = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChatThreats", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "KeywordTemplates",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    title = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    keywords = table.Column<string>(type: "nvarchar(1550)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KeywordTemplates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PromoCodes",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    code = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    discountPercentage = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    maximumLimit = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    validity = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromoCodes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VendorDocs",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    vendorId = table.Column<long>(type: "bigint", nullable: false),
                    document = table.Column<string>(type: "nvarchar(1500)", nullable: true),
                    type = table.Column<string>(type: "nvarchar(150)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VendorDocs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VendorDocs_Vendors_vendorId",
                        column: x => x.vendorId,
                        principalTable: "Vendors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VendorPartners",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    email = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    emiratesId = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    vendorId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VendorPartners", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VendorPartners_Vendors_vendorId",
                        column: x => x.vendorId,
                        principalTable: "Vendors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Wishlists",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    userId = table.Column<long>(type: "bigint", nullable: true),
                    productId = table.Column<long>(type: "bigint", nullable: false),
                    deviceId = table.Column<string>(type: "nvarchar(550)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Wishlists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Wishlists_Products_productId",
                        column: x => x.productId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Wishlists_Users_userId",
                        column: x => x.userId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Chats_orderId",
                table: "Chats",
                column: "orderId");

            migrationBuilder.CreateIndex(
                name: "IX_Chats_userId",
                table: "Chats",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_Chats_vendorId",
                table: "Chats",
                column: "vendorId");

            migrationBuilder.CreateIndex(
                name: "IX_VendorDocs_vendorId",
                table: "VendorDocs",
                column: "vendorId");

            migrationBuilder.CreateIndex(
                name: "IX_VendorPartners_vendorId",
                table: "VendorPartners",
                column: "vendorId");

            migrationBuilder.CreateIndex(
                name: "IX_Wishlists_productId",
                table: "Wishlists",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "IX_Wishlists_userId",
                table: "Wishlists",
                column: "userId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Chats");

            migrationBuilder.DropTable(
                name: "ChatThreats");

            migrationBuilder.DropTable(
                name: "KeywordTemplates");

            migrationBuilder.DropTable(
                name: "PromoCodes");

            migrationBuilder.DropTable(
                name: "VendorDocs");

            migrationBuilder.DropTable(
                name: "VendorPartners");

            migrationBuilder.DropTable(
                name: "Wishlists");

            migrationBuilder.DropColumn(
                name: "keywords",
                table: "Products");
        }
    }
}
