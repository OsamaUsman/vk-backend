﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VirtualKitchenCore.Migrations
{
    public partial class someadditions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Users_UserId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_UserId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Users");

            migrationBuilder.RenameColumn(
                name: "ShopName",
                table: "Vendors",
                newName: "shopName");

            migrationBuilder.AddColumn<long>(
                name: "userId",
                table: "Vendors",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vendors_userId",
                table: "Vendors",
                column: "userId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vendors_Users_userId",
                table: "Vendors",
                column: "userId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vendors_Users_userId",
                table: "Vendors");

            migrationBuilder.DropIndex(
                name: "IX_Vendors_userId",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "userId",
                table: "Vendors");

            migrationBuilder.RenameColumn(
                name: "shopName",
                table: "Vendors",
                newName: "ShopName");

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "Users",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserId",
                table: "Users",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Users_UserId",
                table: "Users",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
