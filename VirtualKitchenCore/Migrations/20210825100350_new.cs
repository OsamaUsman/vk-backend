﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VirtualKitchenCore.Migrations
{
    public partial class @new : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetails_Users_userId",
                table: "OrderDetails");

            migrationBuilder.DropIndex(
                name: "IX_OrderDetails_userId",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "address",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "addressId",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "addressTitle",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "contactNo",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "deliverToMe",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "latitude",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "longitude",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "name",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "pickupTime",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "userId",
                table: "OrderDetails");

            migrationBuilder.RenameColumn(
                name: "streetNo",
                table: "OrderDetails",
                newName: "quantity");

            migrationBuilder.AddColumn<string>(
                name: "address",
                table: "Orders",
                type: "nvarchar(500)",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "addressId",
                table: "Orders",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "addressTitle",
                table: "Orders",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "contactNo",
                table: "Orders",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "latitude",
                table: "Orders",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "longitude",
                table: "Orders",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "name",
                table: "Orders",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "streetNo",
                table: "Orders",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "addOns",
                table: "OrderDetails",
                type: "nvarchar(2000)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "variants",
                table: "OrderDetails",
                type: "nvarchar(2000)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "address",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "addressId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "addressTitle",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "contactNo",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "latitude",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "longitude",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "name",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "streetNo",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "addOns",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "variants",
                table: "OrderDetails");

            migrationBuilder.RenameColumn(
                name: "quantity",
                table: "OrderDetails",
                newName: "streetNo");

            migrationBuilder.AddColumn<string>(
                name: "address",
                table: "OrderDetails",
                type: "nvarchar(500)",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "addressId",
                table: "OrderDetails",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "addressTitle",
                table: "OrderDetails",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "contactNo",
                table: "OrderDetails",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "deliverToMe",
                table: "OrderDetails",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "latitude",
                table: "OrderDetails",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "longitude",
                table: "OrderDetails",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "name",
                table: "OrderDetails",
                type: "nvarchar(150)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickupTime",
                table: "OrderDetails",
                type: "nvarchar(250)",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "userId",
                table: "OrderDetails",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_userId",
                table: "OrderDetails",
                column: "userId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetails_Users_userId",
                table: "OrderDetails",
                column: "userId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
