﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VirtualKitchenCore.Migrations
{
    public partial class phase1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FoodCategories",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    sequence = table.Column<int>(type: "int", nullable: false),
                    imageUrl = table.Column<string>(type: "nvarchar(1000)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IngredientCategories",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    imageUrl = table.Column<string>(type: "nvarchar(1000)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IngredientCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    userName = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    firstName = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    lastName = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    email = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    password = table.Column<string>(type: "nvarchar(200)", nullable: true),
                    mobileno = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    role = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    status = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    suspendedTill = table.Column<DateTime>(type: "datetime2", nullable: false),
                    verificationCode = table.Column<long>(type: "bigint", nullable: false),
                    UserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Vendors",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    firstName = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    lastName = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    email = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    tradeLicense = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    status = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    suspendedTill = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ShopName = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    mobileNo = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    address = table.Column<string>(type: "nvarchar(200)", nullable: true),
                    city = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    country = table.Column<string>(type: "nvarchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vendors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    description = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    coverImgUrl = table.Column<string>(type: "nvarchar(1000)", nullable: true),
                    cost = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    minimumTime = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    calories = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    variant = table.Column<string>(type: "nvarchar(1500)", nullable: true),
                    foodCatId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_FoodCategories_foodCatId",
                        column: x => x.foodCatId,
                        principalTable: "FoodCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    title = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    mapAddress = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    latitude = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    longitude = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    deviceId = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    addressOne = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    city = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    userId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Addresses_Users_userId",
                        column: x => x.userId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "KitchenCategories",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    status = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    vendorId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KitchenCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_KitchenCategories_Vendors_vendorId",
                        column: x => x.vendorId,
                        principalTable: "Vendors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    modeOfpayment = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    bill = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    vat = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    deliveryCharges = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    totalBill = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    discount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    netBill = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    url = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    transactionId = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    status = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    paidAmount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    deliveryModeId = table.Column<long>(type: "bigint", nullable: false),
                    note = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    userId = table.Column<long>(type: "bigint", nullable: true),
                    vendorId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Users_userId",
                        column: x => x.userId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Vendors_vendorId",
                        column: x => x.vendorId,
                        principalTable: "Vendors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Carts",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    userId = table.Column<long>(type: "bigint", nullable: false),
                    productId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Carts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Carts_Products_productId",
                        column: x => x.productId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Carts_Users_userId",
                        column: x => x.userId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductImages",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    imageUrl = table.Column<string>(type: "nvarchar(1000)", nullable: true),
                    productId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductImages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductImages_Products_productId",
                        column: x => x.productId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Ingredients",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    imageUrl = table.Column<string>(type: "nvarchar(1000)", nullable: true),
                    variants = table.Column<string>(type: "nvarchar(1500)", nullable: true),
                    units = table.Column<string>(type: "nvarchar(1500)", nullable: true),
                    kitchenCategoryId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ingredients", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ingredients_KitchenCategories_kitchenCategoryId",
                        column: x => x.kitchenCategoryId,
                        principalTable: "KitchenCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderDetails",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    userId = table.Column<long>(type: "bigint", nullable: false),
                    orderId = table.Column<long>(type: "bigint", nullable: false),
                    vendorId = table.Column<long>(type: "bigint", nullable: false),
                    productId = table.Column<long>(type: "bigint", nullable: false),
                    date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    status = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    address = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    addressId = table.Column<long>(type: "bigint", nullable: true),
                    cost = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    deliverToMe = table.Column<bool>(type: "bit", nullable: false),
                    latitude = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    longitude = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    productName = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    addressTitle = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    name = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    pickupTime = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    contactNo = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    streetNo = table.Column<string>(type: "nvarchar(150)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderDetails_Orders_orderId",
                        column: x => x.orderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderDetails_Products_productId",
                        column: x => x.productId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderDetails_Users_userId",
                        column: x => x.userId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderDetails_Vendors_vendorId",
                        column: x => x.vendorId,
                        principalTable: "Vendors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_userId",
                table: "Addresses",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_Carts_productId",
                table: "Carts",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "IX_Carts_userId",
                table: "Carts",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_Ingredients_kitchenCategoryId",
                table: "Ingredients",
                column: "kitchenCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_KitchenCategories_vendorId",
                table: "KitchenCategories",
                column: "vendorId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_orderId",
                table: "OrderDetails",
                column: "orderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_productId",
                table: "OrderDetails",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_userId",
                table: "OrderDetails",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_vendorId",
                table: "OrderDetails",
                column: "vendorId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_userId",
                table: "Orders",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_vendorId",
                table: "Orders",
                column: "vendorId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductImages_productId",
                table: "ProductImages",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_foodCatId",
                table: "Products",
                column: "foodCatId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserId",
                table: "Users",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "Carts");

            migrationBuilder.DropTable(
                name: "IngredientCategories");

            migrationBuilder.DropTable(
                name: "Ingredients");

            migrationBuilder.DropTable(
                name: "OrderDetails");

            migrationBuilder.DropTable(
                name: "ProductImages");

            migrationBuilder.DropTable(
                name: "KitchenCategories");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Vendors");

            migrationBuilder.DropTable(
                name: "FoodCategories");
        }
    }
}
