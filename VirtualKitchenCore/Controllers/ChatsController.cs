﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Repo;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChatsController : GenController<Chat,ChatRepo>
    {
        ChatRepo repo;
        public ChatsController(ChatRepo repo) : base(repo)
        {
            this.repo = repo;
        }

        [HttpGet("ThreatsByVendor/{id}")]
        public async Task<IActionResult> ThreatsByVendor(long id)
        {
            var row = await repo.ThreatsByVendor(id);
            return Ok(row);
        }
        [HttpGet("GetAllThreats")]
        public async Task<IActionResult> GetAllThreats()
        {
            var row = await repo.GetAllThreats();
            return Ok(row);
        }

        [HttpGet("ChatByOrder/{id}")]
        public async Task<IActionResult> ChatByOrder(long id)
        {
            var row = await repo.ChatByOrder(id);
            return Ok(row);
        }
        [HttpPost("AddChat")]
        public async Task<IActionResult> AddChat(Chat chat)
        {
            var row = await repo.AddChat(chat);
            return Ok(row);
        }
    }
}
