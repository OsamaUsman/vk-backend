﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Repo;
using VirtualKitchenCore.Models.Tables;
using VirtualKitchenCore.Models.ViewModels;

namespace VirtualKitchenCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController :  GenController<Order, OrderRepo>
    {
        OrderRepo repo;
        public OrdersController(OrderRepo repo) : base(repo)
        {
            this.repo = repo;
        }
        [HttpGet("ReOrder/{id}/{userId}")]
        public async Task<IActionResult> ReOrder(long id,long userId)
        {
            return Ok(await repo.ReOrder(id,userId));
        }
        [HttpGet("History/{id}")]
        public async Task<IActionResult> History(long id)
        {
            return Ok(await repo.History(id));
        }
        [HttpGet("Inprogress/{id}")]
        public async Task<IActionResult> Inprogress(long id)
        {
            return Ok(await repo.Inprogress(id));
        }
        [HttpPost("PlaceOrder")]
        public async Task<IActionResult> PlaceOrder(OrderViewModel vm)
        {
            return Ok(await repo.PlaceOrder(vm));
        }
        [HttpGet("GetOrders/{id}")]
        public async Task<IActionResult> GetOrders(long id)
        {
            return Ok(await repo.GetOrders(id));
        }
        [HttpGet("GetOrderDetails/{id}")]
        public async Task<IActionResult> GetOrderDetails(long id)
        {
            return Ok(await repo.GetOrderDetails(id));
        }
        [HttpPost("FilterOrders/{id}")]
        public async Task<IActionResult> FilterOrders(long id,FilterViewModel vm)
        {
            return Ok(await repo.FilterOrders(id, vm));
        }
        [HttpGet("Getall")]
        public async Task<IActionResult> Getall()
        {
            return Ok(await repo.Getall());
        }

    }
}
