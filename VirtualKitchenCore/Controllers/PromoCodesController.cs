﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Repo;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromoCodesController : GenController<PromoCode,PromoCodeRepo>
    {
        PromoCodeRepo repo;
        public PromoCodesController(PromoCodeRepo repo) : base(repo)
        {
            this.repo = repo;
        }
        [HttpGet("ValidPromoCode/{code}")]
        public async Task<IActionResult> ValidPromoCode(string code)
        {
            var row = await repo.ValidPromoCode(code);
            return Ok(row);
        }
        [HttpGet("AllValidPromoCode")]
        public async Task<IActionResult> AllValidPromoCode()
        {
            var row = await repo.AllValidPromoCode();
            return Ok(row);
        }
    }
}
