﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Repo;
using VirtualKitchenCore.Models.Tables;
using VirtualKitchenCore.Models.ViewModels;

namespace VirtualKitchenCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartsController : GenController<Cart,CartRepo>
    {
        CartRepo repo;
        public CartsController(CartRepo repo)  : base (repo) 
        {
            this.repo = repo; 
        }
        [HttpGet("Getby/{id}")]
        public async Task<IActionResult> Getby(long id)
        {
            var row = await repo.Getby(id);
            return Ok(row);
        }
        [HttpDelete("Deleteby/{id}")]
        public async Task<IActionResult> Deleteby(long id)
        {
            var row = await repo.Deleteby(id);
            return Ok(row);
        }
        [HttpPut("Edit/{id}")]
        public async Task<IActionResult> Edit(long id, AddCartViewModel cart)
        {
            var row = await repo.Edit(id, cart);
            return Ok(row);
        }
        [HttpPost("Add")]
        public async Task<IActionResult> Add(AddCartViewModel cart)
        {
            var row = await repo.Add(cart);
            return Ok(row);
        }

    }
}
