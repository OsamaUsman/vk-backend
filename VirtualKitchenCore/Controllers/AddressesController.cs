﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Repo;
using VirtualKitchenCore.Models.Tables;
using VirtualKitchenCore.Models.ViewModels;

namespace VirtualKitchenCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddressesController : GenController<Address,AddressRepo>
    {
        AddressRepo repo;
        public AddressesController(AddressRepo repo) : base (repo)
        {
            this.repo = repo; 
        }

        [HttpPost("Get")]
        public async Task<IActionResult> Get(AddressViewModel vm)
        {
            
            return Ok(await repo.Get(vm));
        }
        [HttpPut("Update/{id}")]
        public async Task<IActionResult> Update(long id, Address address)
        {

            return Ok(await repo.Update(id, address));
        }
        [HttpPost("AddAddress")]
        public async Task<IActionResult> AddAddress(Address address)
        {

            return Ok(await repo.AddAddress( address));
        }
        [HttpDelete("Remove/{id}")]
        public async Task<IActionResult> Remove(long id)
        {

            return Ok(await repo.Remove(id));
        }
    }
}
