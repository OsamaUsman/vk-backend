﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Repo;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KeywordTemplatesController : GenController<KeywordTemplate,KeywordTemplateRepo>
    {
        KeywordTemplateRepo repo;
        public KeywordTemplatesController(KeywordTemplateRepo repo) : base(repo)
        {
            this.repo = repo;
        }
    }
}
