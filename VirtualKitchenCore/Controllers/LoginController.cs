﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using VirtualKitchenCore.Models;
using VirtualKitchenCore.Models.Repo;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private IConfiguration configuration;
        private readonly UserRepo us;
        public LoginController(IConfiguration configuration, UserRepo us)
        {
            this.configuration = configuration;
            this.us = us;
        }
        [HttpPost("UserLogin")]
        public IActionResult UserLogin(JWTModel login)
        {
            //JWTModel login = new JWTModel();

            IActionResult response = Unauthorized();

            var user = AuthenticateUser(login);
            if (user != null)
            {
                var tokenstr = GenerateJWTWebToken(user);
                return Ok(new {status="success", token = tokenstr ,data = user});
            }
           
            return Ok(new {status="success",data="not exist" });

        }

        
        [HttpPost("Register")]
        public async Task<ActionResult<object>> Register(User entity)
        {
            
        return  await us.RegisterUser(entity);
           
        }
        private JWTModel AuthenticateUser(JWTModel login)
        {
            //JWTModel user = null;
            var user = us.LoginUser(login);
            if (user != null)
            {
                return user;
            }

            return user;
        }

        private string GenerateJWTWebToken(JWTModel info)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claim = new[]
            {
             new Claim(JwtRegisteredClaimNames.Sub,info.UserName),
             new Claim(JwtRegisteredClaimNames.Email,info.Email),
             new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString(),info.Password),

            };
            var token = new JwtSecurityToken(

                issuer: configuration["Jwt:Issuer"],
                audience: configuration["Jwt:Issuer"],
                claim,
                expires: DateTime.Now.AddYears(1),
                signingCredentials: credentials);
            var encodedtoken = new JwtSecurityTokenHandler().WriteToken(token);
            return encodedtoken;
        }
        [Authorize]
        [HttpPost]
        public string Post()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            IList<Claim> claim = identity.Claims.ToList();
            var userName = claim[0].Value;
            return "Wellcome to username " + claim;
        }
        [Authorize]
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "Value1", "Value1", "Value1" };
        }

    }
}
