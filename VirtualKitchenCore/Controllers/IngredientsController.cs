﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Repo;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IngredientsController : GenController<Ingredient, IngredientRepo>
    {
         IngredientRepo repo;
        public IngredientsController(IngredientRepo repo) : base(repo)
        {
            this.repo = repo;
        }

        [HttpGet("GetIngredient/{id}")]
        public async Task<IActionResult> GetIngredient(long id)
        {
            var row = await repo.GetIngredient(id);
            return Ok(row);
        }

        [HttpGet("GetIngredientByKitchen/{id}")]
        public async Task<IActionResult> GetIngredientByKitchen(long id)
        {
            var row = await repo.GetIngredientByKitchen(id);
            return Ok(row);
        }
        [HttpGet("GetIngredientbyK/{id}")]
        public async Task<IActionResult> GetIngredientbyK(long id)
        {
            var row = await repo.GetIngredientbyK(id);
            return Ok(row);
        }
    }
}
