﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Repo;
using VirtualKitchenCore.Models.Tables;
using VirtualKitchenCore.Models.ViewModels;

namespace VirtualKitchenCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorsController : GenController<Vendor, VendorRepo>
    {
        VendorRepo repo;
        public VendorsController(VendorRepo repo) : base(repo)
        {
            this.repo = repo;
        }

        [AllowAnonymous]
        [HttpPost("Add")]
        public async Task<IActionResult> Add(VendorViewModel vm)
        {
            var row = await repo.Add(vm);
            return Ok(row);
        }

    
        [HttpGet("VendorDetails/{id}")]
        public async Task<IActionResult> VendorDetails(long id)
        {
            var row = await repo.VendorDetails(id);
            return Ok(row);
        }
        [HttpGet("GetAllVendors")]
        public async Task<IActionResult> GetAllVendors()
        {
            var row = await repo.GetAllVendors();
            return Ok(row);
        }
        [HttpPut("Status/{id}/{status}")]
        public async Task<IActionResult> Status(long id,string status)
        {
            var row = await repo.Status(id,status);
            return Ok(row);
        }
    }
}
