﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Repo;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WishlistsController : GenController<Wishlist,WishlistRepo>
    {
        WishlistRepo repo;
        public WishlistsController(WishlistRepo repo) : base (repo)
        {
            this.repo = repo;
        }
    }
}
