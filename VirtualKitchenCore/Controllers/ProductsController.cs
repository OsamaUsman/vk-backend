﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using VirtualKitchenCore.Models.Repo;
using VirtualKitchenCore.Models.Tables;
using VirtualKitchenCore.Models.ViewModels;

namespace VirtualKitchenCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : GenController<Product, ProductRepo>
    {
        ProductRepo repo;
        public ProductsController(ProductRepo repo) : base(repo)
        {
            this.repo = repo;
        }
       // [AllowAnonymous]
        [HttpGet("GetAll/{id}")]
        public async Task<IActionResult> GetAll(long id)
        {
            var row = await repo.GetAll(id);
            return Ok(row);
        }
        [HttpPost("Search")]
        public async Task<IActionResult> Search(SearchViewModel vm)
        {
            var row = await repo.Search(vm);
            return Ok(row);
        }
        [HttpGet("LoadData/{id}/{city}")]
        public async Task<IActionResult> LoadData(long id , string city)
        {
            var row = await repo.LoadData(id ,city);
            return Ok(row);
        }

        [HttpPost("EditAll")]
        public async Task<IActionResult> EditAll(ProductViewModel vm)
        {
            var row = await repo.EditAll(vm);
            return Ok(row);
        }


        [HttpGet("ByVendor/{id}")]
        public async Task<IActionResult> ByVendor(long id)
        {
            var row = await repo.ByVendor(id);
            return Ok(row);
        }
        [HttpPost("Add")]
        public async Task<IActionResult> Add(ProductViewModel vm)
        {
            var row = await repo.Add(vm);
            return Ok(row);
        }
        [HttpDelete("DeleteProduct/{id}")]
        public async Task<IActionResult> DeleteProduct(long id)
        {
            var row = await repo.DeleteProduct(id);
            return Ok(row);
        }
        [HttpGet("ByCategory/{id}/{city}")]
        public async Task<IActionResult> ByCategory(long id,string city)
        {
            var row = await repo.ByCategory(id,city);
            return Ok(row);
        }
        [HttpGet("CityChange/{city}")]
        public async Task<IActionResult> CityChange(string city)
        {
            var row = await repo.CityChange(city);
            return Ok(row);
        }
        [HttpPost("Filter")]
        public async Task<IActionResult> Filter(ProductFilterViewModel vm)
        {
            var row = await repo.Filter(vm);
            return Ok(row);
        }
        [HttpGet("Getall")]
        public async Task<IActionResult> Getall()
        {
            var row = await repo.Getall();
            return Ok(row);
        }
        [HttpPut("Status/{id}/{status}")]
        public async Task<IActionResult> Status(long id, string status)
        {
            var row = await repo.Status(id,status);
            return Ok(row);
        }
    }
}
