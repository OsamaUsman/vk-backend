﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Repo;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodCategoriesController : GenController<FoodCategory, FoodCategoryRepo>
    {
        FoodCategoryRepo repo;
        public FoodCategoriesController(FoodCategoryRepo repo) : base(repo)
        {
            this.repo = repo;
        }
        [HttpDelete("Remove/{id}")]
        public async Task<IActionResult> Remove(long id) 
        {
           return  Ok(await repo.Remove(id));
        }
    }
}
