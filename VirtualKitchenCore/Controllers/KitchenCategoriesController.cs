﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Repo;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KitchenCategoriesController : GenController<KitchenCategory,KitchenCategoryRepo>
    {
        KitchenCategoryRepo repo;
        public KitchenCategoriesController(KitchenCategoryRepo repo) : base (repo)
        {
            this.repo = repo;
        }

        [HttpGet("ByVendor/{id}")]
        public async Task<IActionResult> ByVendor(long id)
        {
            var row = await repo.ByVendor(id);
            return Ok(row);
        }
        [HttpGet("ByCity/{city}")]
        public async Task<IActionResult> ByCity(string city)
        {
            var row = await repo.ByCity(city);
            return Ok(row);
        }
    }

}
