﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Repo;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : GenController<User, UserRepo>
    {
        UserRepo repo;
        public UsersController(UserRepo repo) : base(repo)
        {
            this.repo = repo;
        }

        [AllowAnonymous]
        [HttpGet("Exist/{email}")]
        public async Task<IActionResult> Exist(string email)
        {
            var row = await repo.Exist(email);
            return Ok(row);
        }
        [HttpPut("Profile/{id}")]
        public async Task<IActionResult> Profile(long id, User user)
        {
            var row = await repo.Profile(id, user);
            return Ok(row);
        }
    }
}
