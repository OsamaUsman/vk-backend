﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models
{
    public class JWTModel
    {
        public long Id { get; set; }
        
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public string Role { get; set; }
        public string Logo { get; set; }
        public string Mobile { get; set; }
        public string Status { get; set; }
        public long VendorId { get; set; }

    }
}
