﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.ViewModels
{
    public class FilterViewModel
    {
        public DateTime fromdate { get; set; }
        public DateTime todate { get; set; }
    }
}
