﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.ViewModels
{
    public class ProductViewModel
    {
        public Product product { get; set; }
        public string[] images { get; set; }
        public ICollection<Variant> variants { get; set; }
        public ICollection<AddOn> addons { get; set; }
    }
}
