﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.ViewModels
{
    public class SearchViewModel
    {
        public string city { get; set; }
        public string text { get; set; }
    }
}
