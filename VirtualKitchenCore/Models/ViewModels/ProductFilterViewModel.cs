﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.ViewModels
{
    public class ProductFilterViewModel
    {
        public string city { get; set; }
        public long?[] ids { get; set; }
        public decimal? from { get; set; }
        public decimal? to { get; set; }
    }
}
