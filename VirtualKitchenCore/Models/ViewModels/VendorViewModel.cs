﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.ViewModels
{
    public class VendorViewModel
    {
        public Vendor vendor { get; set; }
        public User user { get; set; }
        public List<VendorPartner> partners  { get; set; }

        public string[] emiratesids { get; set; }
        public string[] productsamples { get; set; }
        public string[] tradelicenses { get; set; }
    }
}
