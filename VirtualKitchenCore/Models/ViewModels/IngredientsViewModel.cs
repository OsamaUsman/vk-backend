﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.ViewModels
{
    public class IngredientsViewModel
    {
        public long Id { get; set; }

        public string name { get; set; }

        public string imageUrl { get; set; }

        public string[] variants { get; set; }

        public string[] units { get; set; }

        public string qty { get; set; }
        public decimal price { get; set; }

        public string standardQty { get; set; }

        public long kitchenCategoryId { get; set; }
    }
}
