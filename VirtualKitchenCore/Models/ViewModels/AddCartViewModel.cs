﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.ViewModels
{
    public class AddCartViewModel
    {
        public long Id { get; set; }

        public long userId { get; set; }
        public long? productId { get; set; }
        public long? vendorId { get; set; }

        public string variants { get; set; }

        public string addOns { get; set; }

        public string type { get; set; }

        public string quantity { get; set; }

        public string[] ingredients { get; set; }

        public decimal amount { get; set; }
        public long? kitchenId { get; set; }
        public long? variantId { get; set; }
        public string addOnsIds { get; set; }
    }
}
