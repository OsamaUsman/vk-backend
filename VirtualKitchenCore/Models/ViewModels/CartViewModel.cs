﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.ViewModels
{
    public class CartViewModel
    {
        public long Id { get; set; }
        public long userId { get; set; }
        public long? productId { get; set; }
        public long? vendorId { get; set; }
        public string[] addOnsList { get; set; }
        public string[] ingredients { get; set; }
        public string AddOns { get; set; }
        public string variants { get; set; }
        public string type { get; set; }
        public string quantity { get; set; }
        public decimal? amount { get; set; }  
        public string addOnsIds { get; set; }
        public long? variantId { get; set; }
        public long? kitchenId { get; set; }
        public string restaurantName { get; set; }
        public string city { get; set; }
        public Product product { get; set; }
    }
}
