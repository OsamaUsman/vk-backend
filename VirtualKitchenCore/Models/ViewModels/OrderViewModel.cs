﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.ViewModels
{
    public class OrderViewModel
    {
        public Order order { get; set; }
        public List<CartViewModel> detailList { get; set; }

    }
}
