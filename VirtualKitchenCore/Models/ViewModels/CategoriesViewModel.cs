﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.ViewModels
{
    public class CategoriesViewModel
    {
        public long Id { get; set; }
        public string name { get; set; }
        public int sequence { get; set; }
        public string imageUrl { get; set; }
        public long totalProducts { get; set; }


    }
}
