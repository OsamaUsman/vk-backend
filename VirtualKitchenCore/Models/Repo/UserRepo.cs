﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.ModelBinding;
using VirtualKitchenCore.Enc;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.Repo
{
    public class UserRepo : Repo<User, VKDbContext>
    {
        VKDbContext db;
        public UserRepo(VKDbContext db) : base(db)
        {
            this.db = db;
        }

        public JWTModel LoginUser(JWTModel model)
        {
            try
            {
                var enc = EncDec.Encrypt(model.Password);
                if (model.Role != null && model.Role == "vendor")
                {
                    var venrow = db.Users.Join(db.Vendors, us => us.Id, ven => ven.userId, (us, ven) => new
                    {
                        us,
                        ven
                    }).Where(u => u.us.email == model.Email && u.us.password == enc).Select(us => new
                    {
                        us.us.userName,
                        us.us.mobileno,
                        us.us.password,
                        us.us.role,
                        us.us.status,
                        us.us.email,
                        us.ven.logo,
                        VendorId = us.ven.Id,
                        UserId = us.us.Id,

                    }).FirstOrDefault();
                    if (venrow == null || venrow.role.ToLower() != "vendor" )
                    {
                        return model = null;
                    }
                    else 
                    {
                        model.UserName = venrow.userName;
                        model.Mobile = venrow.mobileno;
                        model.Password = venrow.password;
                        model.Role = venrow.role;
                        model.Status = venrow.status;
                        model.Email = venrow.email;
                        model.Logo = venrow.logo;
                        model.VendorId = venrow.VendorId;
                        model.Id = venrow.UserId;

                        return model;
                    }
                    
                }

                else
                {
                    var row = db.Users.Where(u => u.email == model.Email && u.password == enc).FirstOrDefault();
                    if (row == null || row.role.ToLower() != "user")
                    {
                        return model = null;
                    }
                    else 
                    {
                        model.UserName = row.userName;
                        model.Mobile = row.mobileno;
                        model.Password = row.password;
                        model.Role = row.role;
                        model.Status = row.status;
                        model.Email = row.email;
                        model.Id = row.Id;
                        return model;
                    }
                    



                }

            }
            catch (Exception)
            {

                throw;
            }
       
        }

        public async Task<object> RegisterUser(User entity)
         {
            try
            {
                if (entity.userName == null || entity.userName == "" || entity.password == null || entity.password == "" || entity.role == null ||
                    entity.role == "" || entity.email == null || entity.email == "")
                {
                    return new { status = "error", data = "Username ,email, password, and role are mandatory." };

                }
                else 
                {
                    var row = db.Users.Where(us => us.email == entity.email).FirstOrDefault();
                    if (row != null)
                    {
                        return new { status = "success", data = row.email + " already exist" };
                    }
                    else
                    {
                        var enc = EncDec.Encrypt(entity.password);
                        entity.password = enc;
                        db.Users.Add(entity);
                        await db.SaveChangesAsync();
                        return new { status = "success", data = entity };
                    }
                }
              

            }
            catch (Exception)
            {

                throw;
            }
                       
        }
        public async Task<object> Exist(string email) 
        {
            var row =  db.Users.Where(us => us.email == email && us.role == "vendor").FirstOrDefault();

            if (row == null)
            {
                return new { data = "notExist", status = "success" };
            }
            else
            {
                return new { data = "isExist", status = "error" };
            }
        }
        public async Task<object> Profile(long id , User user)
        {
            var row = await db.Users.AsNoTracking().Where(us => us.Id == id  && us.role.ToLower() == "user").FirstOrDefaultAsync();
            
            if (row == null)
            {
                return new { status = "error", data = "something wenr wrong", };
            }
            else
            {
                User us = new User();
                us.Id = user.Id;
                us.email = user.email;
                us.lastName = user.lastName;
                us.firstName = user.lastName;
                us.mobileno = user.mobileno;
                us.role = row.role;
                us.status = row.status;
                us.userName = user.userName;
                us.verificationCode = user.verificationCode;
                us.password = row.password;
                db.Entry(us).State = EntityState.Modified;
                int check = await db.SaveChangesAsync();
                if (check > 0 )
                {
                    return new { status = "success", data = "updated", };

                }
                else 
                { 
                    return new { status = "error", data = "something wenr wrong", };

                }
            }
        }

    }
}
