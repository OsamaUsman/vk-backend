﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.Repo
{
    public class PromoCodeRepo : Repo<PromoCode,VKDbContext>
    {
        VKDbContext db;
        public PromoCodeRepo(VKDbContext db) : base (db)
        {
            this.db = db;
        }
        public async Task<object> ValidPromoCode(string code) 
        {
            try
            {
                var row = await db.PromoCodes.Where(pm => pm.code == code && pm.validity >= DateTime.Now).ToListAsync();
                if (row != null)
                {
                    return new { status = "success", data = row };
                }
                else 
                {
                    return new { status = "success", data = "Not Found" };
                }
            }
            catch (Exception)
            {

                throw;
            }
           
        }
        public async Task<object> AllValidPromoCode()
        {
            try
            {
                var row = await db.PromoCodes.Where(pm => pm.validity >= DateTime.Now).ToListAsync();
                if (row != null)
                {
                    return new { status = "success", data = row };
                }
                else
                {
                    return new { status = "success", data = "Not Found" };
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
