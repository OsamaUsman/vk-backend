﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;
using VirtualKitchenCore.Models.ViewModels;

namespace VirtualKitchenCore.Models.Repo
{
    public class CartRepo : Repo<Cart, VKDbContext>
    {
        VKDbContext db;
        public CartRepo(VKDbContext db) : base(db)
        {
            this.db = db;
        }
        public async Task<object> Deleteby(long id) 
        {
            try
            {
                db.Carts.RemoveRange(db.Carts.Where(us => us.userId == id));
                int check = await db.SaveChangesAsync();
                if (check > 0)
                {
                    return new { status = "success", data = "deleted" };

                }
                else
                {
                    return new { status = "error", data = "something went wrong." };

                }
            }
            catch (Exception)
            {

                throw;
            }
        

        }
        public async Task<object> Getby(long id)
        {
            try
            {
                var carts = await db.Carts.Join(db.Products,
                    cart => cart.productId,
                    pro => pro.Id,
                    (cart, pro) => new
                    {
                        pro,
                        cart
                    }

                    ).Where(us => us.cart.userId == id).Select(pro => new
                    {
                        pro.cart.Id,
                        pro.cart.userId,
                        pro.cart.productId,
                        pro.cart.variants,
                        pro.cart.addOns,
                        pro.cart.type,
                        pro.cart.quantity,
                        pro.cart.amount,
                        pro.cart.ingredients,
                        pro.cart.kitchenId,
                       vendorid = pro.cart.vendorId,
                        pro.cart.variantId,
                        pro.cart.addOnsIds,
                        pro.pro.name,
                        pro.pro.calories,
                        pro.pro.minimumTime,
                        pro.pro.vendorId,
                        pro.pro.description,
                        pro.pro.cost,
                        pro.pro.coverImgUrl,
                        pro.pro.foodCatId
                        
                    }).ToListAsync();

                List<CartViewModel> mycarts = new List<CartViewModel>();
                string[] myarr = new string[1];
                myarr[0] = "";
                foreach (var item in carts)
                {
                    CartViewModel cartu = new CartViewModel();
                    cartu.Id = item.Id;
                    cartu.userId = item.userId;
                    cartu.productId = (long)item.productId;
                    cartu.vendorId = (long)item.vendorId;
                    cartu.quantity = item.quantity;
                    cartu.amount = item.amount;
                    cartu.kitchenId = item.kitchenId;
                    cartu.variants = item.variants;
                    cartu.type = item.type;
                    cartu.addOnsIds = item.addOnsIds;
                    cartu.variantId = item.variantId;
                    cartu.addOnsList = (item.addOns == null) ? cartu.addOnsList = myarr  : item.addOns.Split('~');
                    cartu.AddOns = item.addOns;
                    cartu.ingredients = (item.ingredients == null) ? cartu.ingredients = myarr : item.ingredients.Split('~');
                    Product product = new Product();
                    product.name = item.name;
                    product.calories = item.calories;
                    product.vendorId = item.vendorId;
                    product.minimumTime = item.minimumTime;
                    product.description = item.description;
                    product.cost = item.cost;
                    product.coverImgUrl = item.coverImgUrl;
                    product.foodCatId = item.foodCatId;
                    cartu.product = product;
                    mycarts.Add(cartu);
                    // cartu.AddOns = item.addOns;
                    //var addOns =  item.addOns.Split(",");
                    //item.addOns.ToArray() = addOns.ToArray();  
                    //carts.Add();

                }
                return new { status = "success", data = mycarts };

            }
            catch (Exception)
            {

                throw;
            }

        }
        public async Task<object> Edit(long id , AddCartViewModel cart)
        {
            try
            {
                Cart mycart = new Cart();

                mycart.Id = cart.Id;
                mycart.amount = cart.amount;
                string myingredients = (cart.ingredients == null) ? null : string.Join("~", cart.ingredients).ToString();
                mycart.kitchenId = cart.kitchenId;
                mycart.ingredients = myingredients;
                mycart.vendorId = cart.vendorId;
                mycart.productId = cart.productId;
                mycart.addOns = cart.addOns;
                mycart.quantity = cart.quantity;
                mycart.type = cart.type;
                mycart.userId = cart.userId;
                mycart.variants = cart.variants;
                mycart.variantId = cart.variantId;
                mycart.addOnsIds = cart.addOnsIds;
                db.Entry(mycart).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                int check = await db.SaveChangesAsync();
                //Cart mycart = new Cart();
               // mycart = cart;
                if (check > 0)
                {
                    return new { status = "success", data = mycart };

                }
                else 
                {
                    return new { status = "error", data = "something went wrong." };
                }
            }
            catch (Exception)
            {

                throw;
            }
           
        }
        public async Task<object> Add(AddCartViewModel cart)
        {
            try
            {
                Cart mycart = new Cart();

                mycart.amount = cart.amount;
                string myingredients = (cart.ingredients == null) ? null : string.Join("~", cart.ingredients).ToString();
                mycart.ingredients =  myingredients;
                mycart.productId = cart.productId;
                mycart.kitchenId = cart.kitchenId;
                mycart.vendorId = cart.vendorId;
                mycart.addOns = cart.addOns;
                mycart.quantity = cart.quantity;
                mycart.type = cart.type;
                mycart.userId = cart.userId;
                mycart.variants = cart.variants;
                mycart.variantId = cart.variantId;
                mycart.addOnsIds = cart.addOnsIds;
                db.Carts.Add(mycart);
                int check = await db.SaveChangesAsync();
                //mycart = cart;
                if (check > 0)
                {
                    return new { status = "success", data = mycart };

                }
                else
                {
                    return new { status = "error", data = "something went wrong." };
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
