﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.Repo
{
    public class VendorDocRepo : Repo<VendorDoc,VKDbContext>
    {
        VKDbContext db;
        public VendorDocRepo(VKDbContext db) :base(db)
        {
            this.db = db;
        }
    }
}
