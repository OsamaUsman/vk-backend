﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;
using VirtualKitchenCore.Models.ViewModels;

namespace VirtualKitchenCore.Models.Repo
{
    public class OrderRepo : Repo<Order, VKDbContext>
    {
        VKDbContext db;
        public OrderRepo(VKDbContext db) : base(db)
        {
            this.db = db;
        }
        public async Task<object> History(long id)
        {
            string[] myarr = new string[1];
            var orders = await db.Orders.Where(u => u.userId == id ).Select(ord => new 
            {
                ord.Id,
                ord.userId,
                ord.vendorId,
                ord.name,
                ord.contactNo,
                ord.addressTitle,
                ord.address,
                ord.latitude,
                ord.date,
                ord.longitude,
                ord.modeOfpayment,
                ord.totalBill,
                ord.vat,
                ord.deliveryCharges,
                ord.discount,
                ord.netBill,
                ord.bill,
                ord.status

            }).OrderByDescending(us => us.status == "On going").ThenBy(us => us.status == "completed").ToListAsync();
            // var details = await db.Orders.Join(
            //    db.OrderDetails,
            //    ord => ord.Id,
            //    det => det.orderId,
            //    (ord, det) => new
            //    {
            //        det.Id,
            //        ord.userId,
            //        det.cost,
            //        det.productId,
            //        det.productName,
            //        det.orderId,
            //        det.addOns,
            //        det.variants,
            //        det.quantity,
            //        det.type,
            //        det.ingredients,
            //        det.kitchenId,
            //        det.variantId,
            //        det.addOnsIds,
            //        det.vendorId,
            //        detailStatus = det.status,
            //        //det.vendorId
            //    }
            //).Where(ord => ord.userId == id ).ToListAsync();
            var myorders = await (from det in db.OrderDetails
                                  join pro in db.Products on det.productId equals pro.Id into MyOrders
                                  from pros in MyOrders.DefaultIfEmpty()
                                  select new
                                  {
                                      det.Id,
                                      det.cost,
                                      det.productId,
                                      det.productName,
                                      det.orderId,
                                      det.addOns,
                                      det.variants,
                                      det.quantity,
                                      det.type,
                                      det.ingredients,
                                      det.kitchenId,
                                      det.variantId,
                                      det.addOnsIds,
                                      det.vendorId,

                                      name = pros.name ?? String.Empty,
                                      calories = pros.calories ?? String.Empty,
                                      description = pros.description ?? String.Empty,
                                      coverImgUrl = pros.coverImgUrl ?? String.Empty,
                                      minimumTime = pros.minimumTime ?? String.Empty,
                                      status = pros.status ?? String.Empty,

                                      foodCatId = (pros.foodCatId == null) ? null : pros.foodCatId,
                                      //productId = (pros.Id == null) ? null : pros.Id, 



                                  }).ToListAsync();


          //  var details = await db.Orders.Join(
          //    db.OrderDetails,
          //    ord => ord.Id,
          //    det => det.orderId,
          //    (ord, det) => new
          //    {
          //        ord,
          //        det
          //    }
          //).Where(ord => ord.ord.userId == id).Join(
          //    db.Products,
          //    det => det.det.productId,
          //    pro => pro.Id,
          //    (det, pro) => new
          //    {
          //        det.det.Id,
          //        det.det.cost,
          //        det.det.productId,
          //        det.det.productName,
          //        det.det.orderId,
          //        det.det.addOns,
          //        det.det.variants,
          //        det.det.quantity,
          //        det.det.type,
          //        det.det.ingredients,
          //        det.det.kitchenId,
          //        det.det.variantId,
          //        det.det.addOnsIds,
          //        det.det.vendorId,
          //        pro.coverImgUrl  
          //        //det.vendorId
          //    }
          //).Distinct().ToListAsync();
            List<OrderDetail> detailList = new List<OrderDetail>();
            foreach (var item in myorders)
            {
                OrderDetail det = new OrderDetail();

                det.Id = item.Id;
                det.cost = item.cost;
                det.productId = item.productId;
                det.productName = item.productName;
                det.orderId = item.orderId;
                det.addOns = item.addOns;
                det.ingredientsList = (item.ingredients == null) ? det.ingredientsList = myarr : item.ingredients.Split("~");
                det.addOnsIds = item.addOnsIds;
                det.addOnsList = (item.addOns == null) ? det.addOnsList = myarr : item.addOns.Split("~");
                det.kitchenId = item.kitchenId;
                det.productImage = item.coverImgUrl;
                det.quantity = item.quantity;
                det.type = item.type;
                det.variantId = item.variantId;
                det.variants = item.variants;
                det.vendorId = item.vendorId;

                detailList.Add(det);
            }
            return new { status = "success", orders, detailList };
            //  var products = db.Orders.Where(u => u.userId == id && u.status == "completed").ToList(); 
        }

        public async Task<object> Inprogress(long id)
        {
            var orders = await db.Orders.Where(u => u.userId == id && u.status != "completed").Select(ord => new 
            {
                ord.Id,
                ord.userId,
                ord.vendorId,
                ord.name,
                ord.contactNo,
                ord.addressTitle,
                ord.address,
                ord.latitude,
                ord.longitude,
                ord.modeOfpayment,
                ord.totalBill,
                ord.vat,
                ord.deliveryCharges,
                ord.discount,
                ord.netBill,
                ord.bill

                
                }).ToListAsync();
            var details = await db.Orders.Join(
               db.OrderDetails,
               ord => ord.Id,
               det => det.orderId,
               (ord, det) => new
               {
                   det.Id,
                   ord.userId,
                   orderStatus = ord.status,
                   det.cost,
                   det.productId,
                   det.productName,
                   det.orderId,
                   det.addOns,
                   det.type,
                   det.variants,
                   det.quantity,
                   detailStatus = det.status,
                   detailVendorId = det.vendorId
               }
           ).Where(ord => ord.userId == id && ord.orderStatus != "completed").ToListAsync();

            var products = await db.Orders.Join(
              db.OrderDetails,
              ord => ord.Id,
              det => det.orderId,
              (ord, det) => new
              {
                  ord,
                  det
              }
          ).Where(ord => ord.ord.userId == id && ord.ord.status != "completed").Join(
              db.Products,
              det => det.det.productId,
              pro => pro.Id,
              (det, pro) => new
              {
                  pro
              }
          ).Distinct().ToListAsync();
            return new { status = "success", orders, details, products };
            //  var products = db.Orders.Where(u => u.userId == id && u.status == "completed").ToList(); 
        }

        public async Task<object> PlaceOrder(OrderViewModel vm)
        {
            if (vm.order != null && vm.detailList != null)
            {
                try
                {

                    await db.Orders.AddAsync(vm.order);

                    int check = await db.SaveChangesAsync();

                    if (check > 0)
                    {

                        long Id = vm.order.Id;
                        for (int i = 0; i < vm.detailList.Count; i++)
                        {
                            //vm.detailList[i].orderId = Id;

                            OrderDetail det = new OrderDetail();
                            det.orderId = Id;
                            det.productId = vm.detailList[i].productId;
                            det.quantity = vm.detailList[i].quantity;
                            det.addOns = vm.detailList[i].AddOns;
                            det.kitchenId = vm.detailList[i].kitchenId;
                            det.cost = (decimal)vm.detailList[i].amount;
                            det.quantity = vm.detailList[i].quantity;
                            det.vendorId = (long)vm.detailList[i].vendorId;
                            det.type = vm.detailList[i].type;
                            det.variants = vm.detailList[i].variants;
                            det.productName = (vm.detailList[i].product == null) ? null : vm.detailList[i].product.name ;
                            det.variantId = vm.detailList[i].variantId;
                            det.addOnsIds = vm.detailList[i].addOnsIds;
                            string myingredients = (vm.detailList[i].ingredients == null) ? null : string.Join("~", vm.detailList[i].ingredients).ToString();
                            det.ingredients = myingredients;

                            await db.OrderDetails.AddAsync(det);
                        }


                        await db.SaveChangesAsync();

                    }
                }
                catch (Exception ex)
                {
                    return ex;
                }


                return new
                {
                    data = "added",
                    status = "success",

                };

            }
            else
            {
                return new
                {
                    data = "something went wrong!",
                    status = "error",

                };

            }
        }

        public async Task<object> GetOrders(long id)
        {
            var row = await db.Orders.Where(ord=> ord.vendorId == id).OrderBy(ord=> ord.date).ToListAsync();
            if (row == null) 
            {
                return new
                {
                    data = "Not Found.",
                    status = "success",

                };
            }
            return new
            {
                data = row,
                status = "success",

            };

        }
        public async Task<object> GetOrderDetails(long id)
        {
            var row = await db.Orders.Join(db.OrderDetails, ord => ord.Id, det => det.orderId, (ord, det) => new { ord, det }).Join(db.Products, det=>det.det.productId, pro => pro.Id, 
                (det,pro) => new { det,pro}).Where(ord => ord.det.det.orderId == id)
                .Select(ord=> new 
                {
                    ord.det.ord.addressId,
                    ord.det.ord.addressTitle,
                    ord.det.ord.bill,
                    ord.det.ord.contactNo,
                    ord.det.ord.date,
                    ord.det.ord.deliveryCharges,
                    ord.det.ord.deliveryModeId,
                    ord.det.ord.discount,
                    ord.det.ord.modeOfpayment,
                    ord.det.ord.name,
                    ord.det.ord.note,
                    ord.det.ord.status,
                    ord.det.ord.streetNo,
                    ord.det.ord.vendorId,
                    
                   
                    ord.det.det.orderId ,
                    ord.det.det.productId,
                    ord.det.det.productName,
                    ord.det.det.quantity,
                    ord.det.det.variants,
                    ord.det.det.addOns,
                    ord.det.det.cost,
                    ord.det.det.type,
                    ord.det.det.kitchenId,
                  
                    ord.pro.foodCatId ,
                    ord.pro.coverImgUrl ,
                    ord.pro.description,
                    ord.pro.minimumTime,
                    Productcost = ord.pro.cost
                     })
                .ToListAsync();
            if (row == null)
            {
                return new
                {
                    data = "Not Found.",
                    status = "success",

                };
            }
            return new
            {
                data = row,
                status = "success",

            };

        }
        public async Task<object> FilterOrders(long id,FilterViewModel vm) 
        {
            if (vm.fromdate != null && vm.todate != null)
            {
                // var addedDate = DateTime.Now.Date.AddDays(10);
                // DateTime? addday = todate;
                DateTime mytodate = vm.todate.AddDays(1);
                // var addday = todate.AddDay(1);
                //object p = addday.AddDays(1);
                var query = (from ord in db.Orders

                             join us in db.Users
                             on ord.userId equals us.Id
                             where ord.vendorId == id && ord.date >= vm.fromdate && ord.date <= mytodate

                             select new
                             {

                                 us.userName,
                                 ord.Id,
                                 ord.bill,
                                 ord.discount,
                                 ord.modeOfpayment,
                                 ord.vendorId,
                                 ord.deliveryCharges,
                                 ord.date,
                                 ord.netBill,
                                 ord.totalBill,
                                 ord.status,
                                 ord.vat,
                                 ord.transactionId,
                                 ord.url,
                                 ord.note,
                                 ord.deliveryModeId,
                                 ord.paidAmount,
                                 ord.userId
                             }).ToList();
           
                return new { status = "success", query };
            }
            else
            {
                var query = (from ord in db.Orders

                             join us in db.Users
                             on ord.userId equals us.Id
                             where ord.vendorId == id

                             select new
                             {

                                 us.userName,
                                 ord.Id,
                                 ord.bill,
                                 ord.discount,
                                 ord.modeOfpayment,
                                 ord.vendorId,
                                 ord.deliveryCharges,
                                 ord.date,
                                 ord.netBill,
                                 ord.totalBill,
                                 ord.status,
                                 ord.vat,
                                 ord.transactionId,
                                 ord.url,
                                 ord.note,
                                 ord.deliveryModeId,
                                 ord.paidAmount,
                                 ord.userId
                             }).ToList();
                if (query.Count() == 0)
                {
                    return new { status = "success", query };

                }
                 return new { status = "success", query };
                ;
            }

        }
        public async Task<object> ReOrder(long id, long userId)
        {
            try
            {
                var orderDetails = await db.OrderDetails.Where(ord => ord.orderId == id).ToListAsync();
               if (orderDetails != null)
                {
                    List<Cart> cartList = new List<Cart>();
                    foreach (var item in orderDetails)
                    {
                        Cart mycart = new Cart();
                        if (item.type.ToLower() == "kitchen")
                        {
                            string[] ingredients = item.ingredients.Split('~');
                            // string[] ingredientsIds = new string[];
                            List<long> mylist = new List<long>();

                            string[] ids = new string[10];
                            for (int i = 0; i < ingredients.Length; i++)
                            {
                                 ids = ingredients[i].Split('-');
                                // mylist[i] = ids[0];
                                mylist.Add(long.Parse(ids[0]));
                            }

                            var myingredients = await db.Ingredients.Where(add => mylist.Contains(add.Id)).ToListAsync();
                            decimal? ingamount = 0;
                            List<string> ingString = new List<string>();
                            foreach (var item4 in myingredients)
                            {
                                ingamount = ingamount + item4.price;
                                ingString.Add(ids[0] + "-" + ids[1] + "-" + ids[2] + "-" + item4.price + "-" + ids[4] + "-"  + ids[5] + "-" + ids[6] + "-" + ids[7]);
                            }
                            mycart.userId = userId;
                            mycart.productId = null;
                            mycart.addOns = item.addOns;
                            mycart.variants = item.variants;
                            mycart.type = item.type;
                            mycart.quantity = item.quantity;
                            mycart.amount = (decimal)ingamount;
                            mycart.ingredients = string.Join('~', ingString);
                            mycart.kitchenId = item.kitchenId;
                            mycart.vendorId = item.vendorId;
                            mycart.addOnsIds = null;
                            mycart.variantId = item.variantId;
                            cartList.Add(mycart);

                        }
                        else
                        {
                            string[] addonsIds =  item.addOnsIds.Split(',');
                            decimal? amount = 0;
                            List<string> addonString = new List<string>();
                            Variant variant = new Variant() ;

                            if (item.addOnsIds != "")
                            { 
                            long[] longs = Array.ConvertAll(addonsIds, s => long.Parse(s));
                            var addons = await db.AddOns.Where(add => longs.Contains(add.Id)).ToListAsync();
                                foreach (var item2 in addons)
                                {
                                    amount = amount + item2.addedPrice;
                                    addonString.Add(item2.name + "-AED " + item2.addedPrice);
                                }
                            }

                            if (item.variantId != null) 
                            {
                                 variant = await db.Variants.Where(va => va.Id == item.variantId).FirstOrDefaultAsync();
                                amount = amount + variant.addedPrice;

                            }
                            var product = await db.Products.Where(pro => pro.Id == item.productId).FirstOrDefaultAsync();
                            amount = amount + product.cost;
                            //if (item.type.ToLower() == "kitchen")
                            //{
                            //    var ingredients = await db.Ingredients.Where(va => va.Id == item.variantId).FirstOrDefaultAsync();

                            //}
                            //else 
                            //{
                            //}



                            mycart.userId = userId;
                            mycart.productId = item.productId;
                            mycart.addOns = string.Join("~", addonString);
                            mycart.variants = (item.variantId == null) ? null : variant.name + "-AED " + variant.addedPrice;
                            mycart.type = item.type;
                            mycart.quantity = item.quantity;
                            mycart.amount = (decimal)amount;
                            mycart.ingredients = item.ingredients;
                            mycart.kitchenId = item.kitchenId;
                            mycart.vendorId = item.vendorId;
                            mycart.addOnsIds = item.addOnsIds;
                            mycart.variantId = item.variantId;

                            cartList.Add(mycart);
                        }
                      
                       


                    }
                    await db.Carts.AddRangeAsync(cartList);
                    int check = await db.SaveChangesAsync();
                    string[] myarr = new string[1];
                    myarr[0] = "";
                    List<CartViewModel> mycarts = new List<CartViewModel>();
                    foreach (var item in cartList)
                    {
                        CartViewModel cartu = new CartViewModel();
                        cartu.Id = item.Id;
                        cartu.userId = item.userId;
                        cartu.productId = (item.productId == null) ? null : item.productId;
                        cartu.vendorId = (item.vendorId == null) ? null : item.vendorId;
                        cartu.quantity = item.quantity;
                        cartu.amount = (item.amount == null) ? null : item.amount;
                        cartu.variants = item.variants;
                        cartu.kitchenId = item.kitchenId;
                        cartu.variantId = item.variantId;
                        cartu.addOnsIds = item.addOnsIds;
                        cartu.type = item.type;
                        cartu.addOnsList = (item.addOns == null) ? cartu.addOnsList = myarr : item.addOns.Split("~");
                        cartu.AddOns = item.addOns;
                        cartu.ingredients = (item.ingredients == null) ? cartu.ingredients = myarr : item.ingredients.Split("~");
                        Product product = new Product();
                        var pro = db.Products.Where(pro => pro.Id == item.productId).FirstOrDefault();
                        if (pro != null)
                        {
                            product.name = pro.name;
                            product.vendorId = pro.vendorId;
                            product.calories = pro.calories;
                            product.minimumTime = pro.minimumTime;
                            product.description = pro.description;
                            product.cost = pro.cost;
                            product.coverImgUrl = pro.coverImgUrl;
                            product.foodCatId = pro.foodCatId;
                            cartu.product = product;
                        }
                        else 
                        {
                            pro = null;
                        }
                        mycarts.Add(cartu);
                        // cartu.AddOns = item.addOns;
                        //var addOns =  item.addOns.Split(",");
                        //item.addOns.ToArray() = addOns.ToArray();  
                        //carts.Add();
                    }

                    if (check > 0)
                    {
                        return new { status = "success", data = mycarts };

                    }
                    else
                    {
                        return new { status = "error", data = "something went wrong." };
                    }
                }
                else
                {
                    return new { status = "error", data = "something went wrong." };

                }
            }
            catch (Exception)
            {

                throw;
            }
            


            //if (orderDetails != null)
            //{
            //    Cart mycart = new Cart();

            //    //mycart.amount = orderDetails.a;
            //    //string myingredients = (cart.ingredients == null) ? null : string.Join("~", cart.ingredients).ToString();
            //    //mycart.ingredients = myingredients;
            //    //mycart.productId = cart.productId;
            //    //mycart.addOns = cart.addOns;
            //    //mycart.quantity = cart.quantity;
            //    //mycart.type = cart.type;
            //    //mycart.userId = cart.userId;
            //    //mycart.variants = cart.variants;
            //    //db.Carts.Add(mycart);
            //    //int check = await db.SaveChangesAsync();
            //    //mycart = cart;
            //    //if (check > 0)
            //    //{
            //    //    return new { status = "success", data = mycart };

            //    //}
            //    //else
            //    //{
            //    //    return new { status = "error", data = "something went wrong." };
            //    //}
            //}

        }

        public async Task<object> Getall()
        {
            var row = await db.Orders.Join(db.Vendors, ord => ord.vendorId, ven => ven.Id, (ord, ven) => new { ord, ven }).Select(ord=>new 
            {
                ord.ven.restaurantName,
                ord.ord.Id,
                ord.ord.bill,
                ord.ord.discount,
                ord.ord.modeOfpayment,
                ord.ord.vendorId,
                ord.ord.deliveryCharges,
                ord.ord.date,
                ord.ord.netBill,
                ord.ord.totalBill,
                ord.ord.status,
                ord.ord.vat,
                ord.ord.transactionId,
                ord.ord.url,
                ord.ord.note,
                ord.ord.deliveryModeId,
                ord.ord.paidAmount,
                ord.ord.userId,
                ord.ord.name,
                ord.ord.contactNo

            })
                .OrderBy(ord => ord.date).ToListAsync();
            if (row == null)
            {
                return new
                {
                    data =row,
                    status = "success",

                };
            }
            return new
            {
                data = row,
                status = "success",

            };
        }
    }
}
