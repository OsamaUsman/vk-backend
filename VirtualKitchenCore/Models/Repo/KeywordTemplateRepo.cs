﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.Repo
{
    public class KeywordTemplateRepo : Repo<KeywordTemplate,VKDbContext>
    {
        VKDbContext db;
        public KeywordTemplateRepo(VKDbContext db) :base(db)
        {
            this.db = db;
        }
    }
}
