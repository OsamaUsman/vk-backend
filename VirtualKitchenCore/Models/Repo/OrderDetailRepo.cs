﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.Repo
{
    public class OrderDetailRepo : Repo<OrderDetail, VKDbContext>
    {
        VKDbContext db;
        public OrderDetailRepo(VKDbContext db): base (db)
        {
            this.db = db;
        }
    }
}
