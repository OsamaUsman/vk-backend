﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.Repo
{
    public class ChatRepo : Repo<Chat,VKDbContext>
    {
        VKDbContext db;
        public ChatRepo(VKDbContext db) : base(db)
        {
            this.db = db;
        }
        public async Task<object> ThreatsByVendor(long id) 
        {
            try
            {
                var query = (from ch in db.ChatThreats
                             join us in db.Users
                             on ch.userId equals us.Id
                             where ch.vendorId == id
                             select new
                             {
                                 ch.Id,
                                 ch.orderId,
                                 ch.userId,
                                 ch.vendorId,
                                 ch.date,
                                 us.userName,
                             }).OrderByDescending(ch => ch.date).ToList();
                if (query.Count() == 0)
                {
                    return new { status = "success", data = "Not Found" };
                }

                return new { status = "success", data = query };
            }
            catch (Exception)
            {

                throw;
            }
         
        }
        public async Task<object> ChatByOrder(long id)
        {
            try
            {
                var query = (from ch in db.Chats
                             join user in db.Users
                             on ch.userId equals user.Id
                             join ven in db.Vendors
                             on ch.vendorId equals ven.Id
                             where ch.orderId == id
                             select new
                             {
                                 ch.Id,
                                 ch.orderId,
                                 ch.userId,
                                 ch.sentBy,
                                 ch.vendorId,
                                 ch.message,
                                 ch.type,
                                 ch.date,
                                 ch.status,
                                 ven.restaurantName,
                                 ven.logo,
                                 user.userName
                             }).OrderBy(ch => ch.date).ToList();
                if (query.Count() == 0)
                {
                    return new { status = "success", data = query };
                }

                return new { status = "success", data = query };
            }
            catch (Exception)
            {

                throw;
            }

        }

        public async Task<object> AddChat(Chat chat)
        {
            
            chat.status = "sent";

           await db.Chats.AddAsync(chat);
            try
            {
              int check = await db.SaveChangesAsync();
                if (check > 0)
                {
                    return new { status = "success", data = "added" };

                }

                else 
                {
                    return new {status="success", data= "added" };

                }
            }
            catch (Exception ex)
            {
                return ex;
            }
           
        }
        public async Task<object> GetAllThreats() 
        {
            try
            {
                var query = (from ch in db.ChatThreats
                             join us in db.Users
                             on ch.userId equals us.Id
                             select new
                             {
                                 ch.Id,
                                 ch.orderId,
                                 ch.userId,
                                 ch.vendorId,
                                 ch.date,
                                 us.userName,
                             }).OrderByDescending(ch => ch.date).ToList();
                if (query.Count() == 0)
                {
                    return new { status = "success", data = query };
                }

                return new { status = "success", data = query };
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
