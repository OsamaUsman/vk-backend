﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.Repo
{
    public class KitchenCategoryRepo : Repo<KitchenCategory,VKDbContext>
    {
        VKDbContext db;
        public KitchenCategoryRepo(VKDbContext db) : base (db)
        {
            this.db = db;
        }
        public async Task<object> ByVendor(long id)
        {
            var kitchenCategories = await db.KitchenCategories.Where(p => p.vendorId == id).ToListAsync();
            if (kitchenCategories != null)
            {
                return new { status = "success", data = kitchenCategories };
            }
            else
            {
                return new { status = "success", data = "Not Found" };

            }
        }

        public async Task<object> ByCity(string city)
        {
            var kitchenCategories = await db.KitchenCategories.Join(db.Vendors, kit => kit.vendorId, ven => ven.Id, (kit, ven) => new
            {
                kit,
                ven

            }).Where(kit=> kit.ven.city.ToLower() == city.ToLower()).Select(kit =>
            new
            {

                kit.ven.city,
                kit.kit.Id,
                kit.kit.imageUrl,
                kit.kit.name,
                kit.kit.status,
                kit.kit.vendorId,
                
            }).ToListAsync(); 
            if (kitchenCategories != null)
            {
                return new { status = "success", data = kitchenCategories };
            }
            else
            {
                return new { status = "success", data = "Not Found" };

            }
        }

    }
}
