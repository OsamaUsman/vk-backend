﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Repo
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public abstract class GenController<TEntity, TRepository> : ControllerBase
        where TEntity : class
        where TRepository : IRepo<TEntity>
    {
        private readonly TRepository repository;

        public GenController(TRepository repository)
        {
            this.repository = repository;
        }


        // GET: api/[controller]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TEntity>>> Get()
        {
            return Ok(new
            {
                data = await repository.GetAll(),
                status = "success"
            });
        }

        // GET: api/[controller]/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TEntity>> Get(long id)
        {
            var entity = await repository.Get(id);
            if (entity == null)
            {
                return Ok(new
                {
                    data = "Not Found",
                    status = "success"
                });
            }
            //return entity;
            return  Ok(new
            {
                data = entity,
                status = "success"
            });
        }

        // PUT: api/[controller]/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, TEntity entity)
        {
            //if (id != entity.Id)
            //{
            //    return BadRequest();
            //}
            await repository.Update(entity);
            return Ok(new {
            data = "updated",
            status = "success"
            });
        }

        // POST: api/[controller]
        [HttpPost]
        public async Task<ActionResult<TEntity>> Post(TEntity entity)
        {
            await repository.Add(entity);
            /*return entity*/ /*CreatedAtAction("Get", new { id = entity.Id }, entity)*/;
            return Ok(new
            {
                data = "added",
                status = "success"
            });
        }

        // DELETE: api/[controller]/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TEntity>> Delete(long id)
        {
            var entity = await repository.Delete(id);
            if (entity == null)
            {
                return NotFound();
            }
            return Ok(new
            {
                data = "deleted",
                status = "success"
            });
        }
    }
}
