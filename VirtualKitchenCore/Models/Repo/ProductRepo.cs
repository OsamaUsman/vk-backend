﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;
using VirtualKitchenCore.Models.ViewModels;

namespace VirtualKitchenCore.Models.Repo
{
    public class ProductRepo : Repo<Product, VKDbContext>
    {
        VKDbContext db;
        public ProductRepo(VKDbContext db) : base(db)
        {
            this.db = db;
        }
        public List<long?> GetLists(string city)
        {

            var result = (from s in db.OrderDetails
                          join pro in db.Products
                          on s.productId equals pro.Id
                          join ven in db.Vendors
                          on pro.vendorId equals ven.Id
                          where ven.city == city && s.type == "Food"
                          group s by s.productId into g  // group all items by status
                          orderby g.Count() descending // order by count descending
                           
                          select new
                          {
                              g.Key,
                          }) // cast the output
            .Take(5) // take just 5 items
            .ToList();
            List<long?> ids = new List<long?>();
           // long[] ids;
            //for (int i = 0; i < result.Count; i++)
            //{

            //    ids[i] = 
            //}
            foreach (var item in result)
            {
                ids.Add(item.Key);
            }

            return ids;

        }
        public async Task<object> LoadData(long id, string city)
        {
            try
            {
                var categories = await db.FoodCategories.ToListAsync();
                var vendors = await db.Vendors.Where(ven=> ven.city.ToLower() == city.ToLower()).Select(ven=> new { ven.Id, ven.restaurantName ,ven.city}).ToListAsync();

                //var myproduct = db.Products.Join(db.Vendors, pro => pro.vendorId, ven => ven.Id, (pro, ven) => new
                //{
                //    pro,
                //    ven

                //}).Where(pro => pro.ven.city.ToLower() == city.ToLower()).Select(pro =>
                //new
                //{
                //    pro.pro.Id,
                //    pro.pro.foodCatId,
                //    pro.ven.city,
                //}).AsEnumerable();

                List<CategoriesViewModel> mycategories = new List<CategoriesViewModel>();

                foreach (var item in categories)
                {
                    var totalProducts = db.Products.Join(db.Vendors, pro => pro.vendorId, ven => ven.Id, (pro, ven) => new
                    {
                        pro,
                        ven

                    }).Where(pro => pro.pro.foodCatId == item.Id && pro.ven.city.ToLower() == city.ToLower() && pro.pro.status == "true").Select(pro =>
                    new
                    {
                        pro.pro.Id,
                        pro.pro.foodCatId,
                        pro.ven.city,
                    }).Count();

                    CategoriesViewModel vm = new CategoriesViewModel();
                    vm.Id = item.Id;
                    vm.imageUrl = item.imageUrl;
                    vm.name = item.name;
                    vm.sequence = item.sequence;
                    vm.totalProducts = totalProducts;

                    mycategories.Add(vm);
                }

                //total products 

                //foreach (var item in categories)
                //{
                //    catId.Add(item.Id);
                //}
                List<Product> Products = new List<Product>();
                List<long> ProductIds = new List<long>();
                //List<Variant> Variants = new List<Variant>();


                foreach (var item in categories)
                {
                    var myrow = await db.Products.Join(db.Vendors,
                      pro => pro.vendorId,
                      ven => ven.Id,
                      (pro, ven) => new
                      {
                          pro,
                          ven
                      }).Where(pro => pro.pro.foodCatId == item.Id && pro.ven.city.ToLower() == city.ToLower() && pro.pro.status == "true").Select(pro => pro.pro).Take(10).ToListAsync();


                    foreach (var item1 in myrow)
                    {
                        Products.Add(item1);
                    }
                }
                foreach (var item in Products)
                {
                    ProductIds.Add(item.Id);
                }


                //var AddOns =await db.AddOns.Where(add => ProductIds.Contains((long)add.productId)).ToListAsync();

                //var Variants =await db.Variants.Where(add => ProductIds.Contains((long)add.productId)).ToListAsync();


                //var products = await db.Products.Join(db.Vendors,
                //    pro => pro.vendorId,
                //    ven => ven.Id,
                //    (pro, ven) => new
                //    {
                //        pro,
                //        ven
                //    }

                //    ).Where(ven => ven.ven.city.ToLower() == city.ToLower()).Select(pro => new
                //    {
                //        pro.pro.Id,
                //        pro.pro.calories,
                //        pro.pro.cost,
                //        pro.pro.coverImgUrl,
                //        pro.pro.description,
                //        pro.pro.foodCatId,
                //        pro.pro.minimumTime,
                //        pro.pro.name,
                //        pro.pro.vendorId,
                //    }).ToListAsync();
                var newcarts = await (from cart in db.Carts
                               join ven in db.Vendors 
                               on cart.vendorId equals ven.Id
                               where cart.userId == id
                               
                               join pro in db.Products on cart.productId equals pro.Id into MyCarts
                               from pros in MyCarts.DefaultIfEmpty()
                               select new
                               {
                                   cart.Id,
                                   cart.productId,
                                   cart.addOns,
                                   cart.userId,
                                   cart.variants,
                                   cart.type,
                                   cart.quantity,
                                   cart.amount,
                                   cart.ingredients,
                                   cart.kitchenId,
                                   vendorid =cart.vendorId,
                                   cart.variantId,
                                   cart.addOnsIds,
                                   ven.restaurantName,
                                   ven.city,
                                  

                                   name = pros.name ?? String.Empty,
                                   calories = pros.calories ?? String.Empty,
                                   description = pros.description ?? String.Empty,
                                   coverImgUrl = pros.coverImgUrl ?? String.Empty,
                                   minimumTime = pros.minimumTime ?? String.Empty,
                                   vendorId = (pros.vendorId == null) ? null : pros.vendorId,
                                   status = pros.status ?? String.Empty,

                                   cost = (pros.cost == null) ? null : pros.cost,
                                   foodCatId = (pros.foodCatId == null) ? null : pros.foodCatId, 
                                   //productId = (pros.Id == null) ? null : pros.Id, 



                               }).ToListAsync();

                //var carts = await db.Carts.Join(db.Products,
                //    cart => cart.productId,
                //    pro => pro.Id,
                //    (cart, pro) => new
                //    {
                //        pro,
                //        cart
                //    }

                //    ).Where(us => us.cart.userId == id).Select(pro => new
                //    {
                //        pro.cart.Id,
                //        pro.cart.userId,
                //        pro.cart.productId,
                //        pro.cart.addOns,
                //        pro.cart.variants,
                //        pro.cart.type,
                //        pro.cart.quantity,
                //        pro.cart.amount,
                //        pro.cart.ingredients,
                //        pro.cart.kitchenId,
                //        pro.cart.addOnsIds,
                //        vendorid = pro.cart.vendorId,
                //        pro.cart.variantId,
                //        pro.pro.name,
                //        pro.pro.calories,
                //        pro.pro.minimumTime,
                //        pro.pro.vendorId,
                //        pro.pro.description,
                //        pro.pro.cost,
                //        pro.pro.coverImgUrl,
                //        pro.pro.foodCatId
                //    }).ToListAsync();
                string[] myarr = new string[1];
               myarr[0] = "";
                List<CartViewModel> mycarts = new List<CartViewModel>();
                foreach (var item in newcarts)
                {
                    CartViewModel cartu = new CartViewModel();
                    cartu.Id = item.Id;
                    cartu.userId = item.userId;
                    cartu.productId = (item.productId == null) ? null : item.productId;
                    cartu.vendorId = (item.vendorId == null) ? null : item.vendorId;
                    cartu.quantity = item.quantity;
                    cartu.amount = (item.amount == null) ? null : item.amount;
                    cartu.variants = item.variants;
                    cartu.kitchenId = item.kitchenId;
                    cartu.variantId = item.variantId;
                    cartu.addOnsIds = item.addOnsIds;
                    cartu.type = item.type;
                    cartu.addOnsList = (item.addOns == null) ? cartu.addOnsList = myarr : item.addOns.Split("~");
                    cartu.AddOns = item.addOns;
                    cartu.ingredients = (item.ingredients == null) ? cartu.ingredients = myarr : item.ingredients.Split("~");
                    Product product = new Product();
                    product.name = item.name;
                    product.vendorId = item.vendorId;
                    product.calories = item.calories;
                    product.minimumTime = item.minimumTime;
                    product.description = item.description;
                    product.cost = item.cost;
                    product.coverImgUrl = item.coverImgUrl;
                    product.foodCatId = item.foodCatId;
                    cartu.product = product;
                    mycarts.Add(cartu);
                    // cartu.AddOns = item.addOns;
                    //var addOns =  item.addOns.Split(",");
                    //item.addOns.ToArray() = addOns.ToArray();  
                    //carts.Add();
                }


                var cartAddOns = await db.Carts.Join(db.AddOns,
                    cart => cart.productId,
                    add => add.productId,
                    (cart, add) => new
                    {
                        cart,
                        add
                    }

                    ).Where(us => us.cart.userId == id).Select(add => new
                    {
                        add.add.Id,
                        add.add.name,
                        add.add.addedPrice,
                        add.add.productId,

                    }).ToListAsync();
                var cartVariants = await db.Carts.Join(db.Variants,
                 cart => cart.productId,
                 vari => vari.productId,
                 (cart, vari) => new
                 {
                     cart,
                     vari
                 }

                 ).Where(us => us.cart.userId == id).Select(vari => new
                 {
                     vari.vari.Id,
                     vari.vari.name,
                     vari.vari.addedPrice,
                     vari.vari.productId,

                 }).ToListAsync();



                var address = await db.Addresses.Where(us => us.userId == id).ToListAsync();

                var row = GetLists(city);
                var popularSearches = (from pro in db.Products
                                       where row.Contains(pro.Id) && pro.status == "true"
                                       select new
                                       {
                                           pro.calories,
                                           pro.cost,
                                           pro.coverImgUrl,
                                           pro.description,
                                           pro.foodCatId,
                                           pro.Id,
                                           pro.minimumTime,
                                           pro.name,
                                           pro.vendorId,


                                       }).AsEnumerable().OrderBy(pro => row.IndexOf(pro.Id)).ToList();

                return new { status = "success", mycategories, Products, mycarts, vendors, cartAddOns, cartVariants, address, popularSearches };
            }
            catch (Exception)
            {

                throw;
            }
            
        }
        public async Task<object> ByVendor(long id)
        {
            var products = await db.Products.Where(p => p.vendorId == id).ToListAsync();
            if (products != null)
            {
                return new { status = "success", data = products };
            }
            else
            {
                return new { status = "success", data = "Not Found" };

            }
        }

        public async Task<object> GetAll(long id)
        {
            var products = await db.Products.Join(db.FoodCategories, pro => pro.foodCatId, cat => cat.Id, (pro, cat) => new { pro, cat }).Select(pro => new
            {
                pro.pro.Id,
                pro.pro.name,
                pro.pro.minimumTime,
                pro.pro.calories,
                pro.pro.cost,
                pro.pro.coverImgUrl,
                pro.pro.description,
                pro.pro.foodCatId,
                pro.pro.vendorId,
                pro.pro.status,
                category = pro.cat.name

            }).Where(p => p.Id == id).ToListAsync();
            var addons = await db.AddOns.Where(p => p.productId == id).ToListAsync();
            var variants = await db.Variants.Where(p => p.productId == id).ToListAsync();
            var images = await db.ProductImages.Where(p => p.productId == id).ToListAsync();

            if (products != null)
            {
                return new { status = "success", data = products, addons, variants, images };
            }
            else
            {
                return new { status = "success", data = "Not Found" };

            }
        }

        public async Task<object> Add(ProductViewModel vm)
        {
            try
            {
                Product pro = new Product();
                pro = vm.product;
                await db.Products.AddAsync(pro);
                int check = await db.SaveChangesAsync();
                long Id = pro.Id;

                if (check > 0)
                {

                    if (vm.images.Length != 0)
                    {
                        for (int i = 0; i < vm.images.Length; i++)
                        {
                            ProductImage img = new ProductImage();
                            img.productId = Id;
                            img.imageUrl = vm.images[i];
                            db.ProductImages.Add(img);

                        }
                        int chec = await db.SaveChangesAsync();
                        if (chec > 0)
                        {
                            if (vm.variants.Count() != 0 && vm.variants.ElementAt(0).name != "" && vm.variants.ElementAt(0).addedPrice != 0)
                            {
                                foreach (var item in vm.variants)
                                {
                                    Variant vr = new Variant();
                                    vr.productId = Id;
                                    vr.name = item.name;
                                    vr.addedPrice = item.addedPrice;
                                    await db.Variants.AddAsync(vr);
                                }
                                int ch = await db.SaveChangesAsync();
                                if (ch > 0)
                                {
                                    if (vm.addons.Count() != 0 && vm.addons.ElementAt(0).name != "" && vm.addons.ElementAt(0).addedPrice != 0)
                                    {
                                        foreach (var item in vm.addons)
                                        {
                                            AddOn vr = new AddOn();
                                            vr.productId = Id;
                                            vr.name = item.name;
                                            vr.addedPrice = item.addedPrice;
                                            await db.AddOns.AddAsync(vr);
                                        }
                                        int che = await db.SaveChangesAsync();
                                        if (che > 0)
                                        {
                                            return new { status = "success", data = "added" };

                                        }
                                        else
                                        {
                                            return new { status = "error", data = "something went wrong." };

                                        }

                                    }
                                    else
                                    {
                                        return new { status = "success", data = "added" };

                                    }
                                }
                                else
                                {
                                    return new { status = "error", data = "something went wrong." };
                                }
                            }
                            else
                            {
                                return new { status = "success", data = "added" };
                            }
                        }
                        else
                        {

                            return new { status = "error", data = "something went wrong." };
                        }
                    }
                    else
                    {
                        if (vm.variants.Count() != 0 && vm.variants.ElementAt(0).name != "" && vm.variants.ElementAt(0).addedPrice != 0)
                        {
                            foreach (var item in vm.variants)
                            {
                                Variant vr = new Variant();
                                vr.productId = Id;
                                vr.name = item.name;
                                vr.addedPrice = item.addedPrice;
                                await db.Variants.AddAsync(vr);
                            }
                            int ch = await db.SaveChangesAsync();
                            if (ch > 0)
                            {
                                if (vm.addons.Count() != 0 && vm.addons.ElementAt(0).name != "" && vm.addons.ElementAt(0).addedPrice != 0)
                                {
                                    foreach (var item in vm.addons)
                                    {
                                        AddOn vr = new AddOn();
                                        vr.productId = Id;
                                        vr.name = item.name;
                                        vr.addedPrice = item.addedPrice;
                                        await db.AddOns.AddAsync(vr);
                                    }
                                    int che = await db.SaveChangesAsync();
                                    if (che > 0)
                                    {
                                        return new { status = "success", data = "added" };

                                    }
                                    else
                                    {
                                        return new { status = "error", data = "something went wrong." };

                                    }

                                }
                                else
                                {
                                    return new { status = "success", data = "added" };

                                }
                            }
                        }
                        else
                        {
                            if (vm.addons.Count() != 0 && vm.addons.ElementAt(0).name != "" && vm.addons.ElementAt(0).addedPrice != 0)
                            {
                                foreach (var item in vm.addons)
                                {
                                    AddOn vr = new AddOn();
                                    vr.productId = Id;
                                    vr.name = item.name;
                                    vr.addedPrice = item.addedPrice;
                                    await db.AddOns.AddAsync(vr);
                                }
                                int che = await db.SaveChangesAsync();
                                if (che > 0)
                                {
                                    return new { status = "success", data = "added" };

                                }
                                else
                                {
                                    return new { status = "error", data = "something went wrong." };

                                }

                            }
                            else
                            {
                                return new { status = "success", data = "added" };

                            }
                        }
                        return new { status = "success", data = "added" };
                    }

                }
                else
                {
                    return new { status = "error", data = "something went wrong." };
                }
            }
            catch (Exception)
            {

                throw;
            }


        }

        public async Task<object> EditAll(ProductViewModel vm)
        {
            try
            {
                Product pro = new Product();
                pro = vm.product;
                db.Entry(pro).State = EntityState.Modified;
                int check = await db.SaveChangesAsync();
                long Id = pro.Id;

                if (check > 0)
                {
                    db.ProductImages.RemoveRange(db.ProductImages.Where(pro => pro.productId == Id));
                    await db.SaveChangesAsync();
                    if (vm.images.Length != 0)
                    {

                        for (int i = 0; i < vm.images.Length; i++)
                        {
                            ProductImage img = new ProductImage();
                            img.productId = Id;
                            img.imageUrl = vm.images[i];
                            db.ProductImages.Add(img);

                        }
                        int chec = await db.SaveChangesAsync();
                        if (chec > 0)
                        {
                            db.Variants.RemoveRange(db.Variants.Where(pro => pro.productId == Id));
                            await db.SaveChangesAsync();
                            if (vm.variants.Count() != 0 && vm.variants.ElementAt(0).name != "" && vm.variants.ElementAt(0).addedPrice != 0)
                            {

                                foreach (var item in vm.variants)
                                {
                                    Variant vr = new Variant();
                                    vr.productId = Id;
                                    vr.name = item.name;
                                    vr.addedPrice = item.addedPrice;
                                    await db.Variants.AddAsync(vr);
                                }
                                int ch = await db.SaveChangesAsync();
                                if (ch > 0)
                                {
                                    db.AddOns.RemoveRange(db.AddOns.Where(pro => pro.productId == Id));
                                    await db.SaveChangesAsync();
                                    if (vm.addons.Count() != 0 && vm.addons.ElementAt(0).name != "" && vm.addons.ElementAt(0).addedPrice != 0)
                                    {


                                        foreach (var item in vm.addons)
                                        {
                                            AddOn vr = new AddOn();
                                            vr.productId = Id;
                                            vr.name = item.name;
                                            vr.addedPrice = item.addedPrice;
                                            await db.AddOns.AddAsync(vr);
                                        }
                                        int che = await db.SaveChangesAsync();
                                        if (che > 0)
                                        {
                                            return new { status = "success", data = "added" };

                                        }
                                        else
                                        {
                                            return new { status = "error", data = "something went wrong." };

                                        }

                                    }
                                    else
                                    {
                                        return new { status = "success", data = "added" };

                                    }
                                }
                                else
                                {
                                    return new { status = "error", data = "something went wrong." };
                                }
                            }
                            else
                            {

                                db.AddOns.RemoveRange(db.AddOns.Where(pro => pro.productId == Id));
                                await db.SaveChangesAsync();
                                if (vm.addons.Count() != 0 && vm.addons.ElementAt(0).name != "" && vm.addons.ElementAt(0).addedPrice != 0)
                                {
                                    foreach (var item in vm.addons)
                                    {
                                        AddOn vr = new AddOn();
                                        vr.productId = Id;
                                        vr.name = item.name;
                                        vr.addedPrice = item.addedPrice;
                                        await db.AddOns.AddAsync(vr);
                                    }
                                    int che = await db.SaveChangesAsync();
                                    if (che > 0)
                                    {
                                        return new { status = "success", data = "added" };

                                    }
                                    else
                                    {
                                        return new { status = "error", data = "something went wrong." };

                                    }

                                }
                                else
                                {
                                    return new { status = "success", data = "added" };

                                }
                            }
                        }
                        else
                        {

                            return new { status = "error", data = "something went wrong." };
                        }
                    }
                    else
                    {
                        db.Variants.RemoveRange(db.Variants.Where(pro => pro.productId == Id));
                        await db.SaveChangesAsync();
                        if (vm.variants.Count() != 0 && vm.variants.ElementAt(0).name != "" && vm.variants.ElementAt(0).addedPrice != 0)
                        {

                            foreach (var item in vm.variants)
                            {
                                Variant vr = new Variant();
                                vr.productId = Id;
                                vr.name = item.name;
                                vr.addedPrice = item.addedPrice;
                                await db.Variants.AddAsync(vr);
                            }
                            int ch = await db.SaveChangesAsync();

                            if (ch > 0)
                            {
                                db.AddOns.RemoveRange(db.AddOns.Where(pro => pro.productId == Id));
                                await db.SaveChangesAsync();
                                if (vm.addons.Count() != 0 && vm.addons.ElementAt(0).name != "" && vm.addons.ElementAt(0).addedPrice != 0)
                                {
                                    foreach (var item in vm.addons)
                                    {
                                        AddOn vr = new AddOn();
                                        vr.productId = Id;
                                        vr.name = item.name;
                                        vr.addedPrice = item.addedPrice;
                                        await db.AddOns.AddAsync(vr);
                                    }
                                    int che = await db.SaveChangesAsync();
                                    if (che > 0)
                                    {
                                        return new { status = "success", data = "added" };

                                    }
                                    else
                                    {
                                        return new { status = "error", data = "something went wrong." };

                                    }

                                }
                                else
                                {
                                    return new { status = "success", data = "added" };

                                }
                            }
                        }
                        else
                        {
                            db.AddOns.RemoveRange(db.AddOns.Where(pro => pro.productId == Id));
                            await db.SaveChangesAsync();
                            if (vm.addons.Count() != 0 && vm.addons.ElementAt(0).name != "" && vm.addons.ElementAt(0).addedPrice != 0)
                            {
                                foreach (var item in vm.addons)
                                {
                                    AddOn vr = new AddOn();
                                    vr.productId = Id;
                                    vr.name = item.name;
                                    vr.addedPrice = item.addedPrice;
                                    await db.AddOns.AddAsync(vr);
                                }
                                int che = await db.SaveChangesAsync();
                                if (che > 0)
                                {
                                    return new { status = "success", data = "added" };

                                }
                                else
                                {
                                    return new { status = "error", data = "something went wrong." };

                                }

                            }
                            else
                            {
                                return new { status = "success", data = "added" };

                            }
                        }
                        return new { status = "success", data = "added" };
                    }

                }
                else
                {
                    return new { status = "error", data = "something went wrong." };
                }
            }
            catch (Exception)
            {

                throw;
            }


        }

        public async Task<object> DeleteProduct(long id)
        {
            var det = await db.OrderDetails.Where(pro => pro.productId == id).FirstOrDefaultAsync();
            if (det == null)
            {
                db.AddOns.RemoveRange(db.AddOns.Where(pro => pro.productId == id));
                db.Variants.RemoveRange(db.Variants.Where(pro => pro.productId == id));
                db.ProductImages.RemoveRange(db.ProductImages.Where(pro => pro.productId == id));
                await db.SaveChangesAsync();

                var row = await db.Products.FindAsync(id);
                if (row != null)
                {
                    db.Products.Remove(row);
                    int ch = await db.SaveChangesAsync();
                    if (ch > 0)
                    {
                        return new { status = "success", data = "deleted" };
                    }
                    else
                    {
                        return new { status = "error", data = "something went wrong." };
                    }
                }
                else
                {
                    return new { status = "error", data = "something went wrong." };

                }



            }
            else
            {
                return new { status = "orderd", data = "Sorry you cannot delete this product. There is an order on going which include this product" };
            }



        }
        public async Task<object> ByCategory(long id,string city)
        {
            try
            {
                var product =  db.Products.Join(db.Vendors, pro => pro.vendorId, ven => ven.Id, (pro, ven) => new
                {
                    pro,
                    ven

                }).Where(pro => pro.pro.foodCatId == id && pro.ven.city.ToLower() == city.ToLower() && pro.pro.status == "true").Select(pro =>
                new 
                {
                    pro.pro.Id,
                    pro.pro.foodCatId,
                    pro.pro.calories,
                    pro.pro.cost,
                    pro.pro.coverImgUrl,
                    pro.pro.description,
                    pro.pro.minimumTime,
                    pro.pro.name,
                    pro.pro.vendorId,
                    pro.ven.city,
                }).AsEnumerable();

                var addOns = db.AddOns.Join(product, add => add.productId, pro => pro.Id, (add, pro) => new
                {
                    add,
                    pro

                }).Select(add =>
                new
                {
                    add.add.Id,
                    add.add.name,
                    add.add.productId,
                    add.add.addedPrice,

                }).AsEnumerable();


                var variants = db.Variants.Join(product, var => var.productId, var => var.Id, (var, pro) => new
                {
                    var,
                    pro

                }).Select(var =>
                new
                {
                    var.var.Id,
                    var.var.name,
                    var.var.productId,
                    var.var.addedPrice,

                }).AsEnumerable();

                //var  = await db.AddOns.Join(product, add => add.productId, pro => pro.Id, (add, pro) => new
                //{
                //    add,
                //    pro

                //}).Select(add =>
                //new
                //{
                //    add.add.Id,
                //    add.add.name,
                //    add.add.productId,
                //    add.add.addedPrice,

                //}).ToListAsync();






                if (product == null)
                {
                    return new { status = "success", data = "Not Found" };

                }
                return new { status = "success", data = product, addOns, variants };
            }
            catch (Exception)
            {

                throw;
            }
  


        }

        public async Task<object> Search(SearchViewModel vm) 
        {
            var myproduct = await db.Vendors.Join(db.Products, ven => ven.Id, pro => pro.vendorId, (ven, pro) => new { ven, pro })
                                           .Where(u => u.ven.city == vm.city && u.pro.status == "true").Select(u => new {
                                               u.pro.Id,
                                               u.pro.name,
                                               u.pro.cost,
                                               u.pro.foodCatId,
                                               u.pro.description,
                                               u.pro.vendorId,
                                               u.pro.calories,
                                               u.pro.coverImgUrl,
                                               u.pro.variant,
                                               u.pro.keywords,
                                               u.pro.minimumTime,
                                               u.pro.status,
                                               
                                           }).ToListAsync();
            // myproduct = myproduct.Where(pro => pro.title.ToLower().Contains(text) ).ToList();
            try
            {
                var searches = (from pro in myproduct

                                where  pro.name.ToLower().Contains(vm.text.ToLower()) && pro.status == "true"

                                select new
                                {
                                    pro.Id,
                                    pro.name,
                                    pro.cost,
                                    pro.foodCatId,
                                    pro.description,
                                    pro.vendorId,
                                    pro.calories,
                                    pro.coverImgUrl,
                                    pro.minimumTime,

                                }).ToList();
                return new {status="success",  data = searches };
            }
            catch (Exception ex)
            {

                return new { ex };
            }
        }
        public async Task<object> CityChange(string city) 
        {
            try
            {
                var categories = await db.FoodCategories.ToListAsync();
                List<CategoriesViewModel> mycategories = new List<CategoriesViewModel>();
                var vendors = await db.Vendors.Where(ven => ven.city.ToLower() == city.ToLower()).Select(ven => new { ven.Id, ven.restaurantName, ven.city }).ToListAsync();

                foreach (var item in categories)
                {
                    var totalProducts = db.Products.Join(db.Vendors, pro => pro.vendorId, ven => ven.Id, (pro, ven) => new
                    {
                        pro,
                        ven

                    }).Where(pro => pro.pro.foodCatId == item.Id && pro.ven.city.ToLower() == city.ToLower() && pro.pro.status == "true").Select(pro =>
                    new
                    {
                        pro.pro.Id,
                        pro.pro.foodCatId,
                        pro.ven.city,
                    }).Count();

                    CategoriesViewModel vm = new CategoriesViewModel();
                    vm.Id = item.Id;
                    vm.imageUrl = item.imageUrl;
                    vm.name = item.name;
                    vm.sequence = item.sequence;
                    vm.totalProducts = totalProducts;

                    mycategories.Add(vm);
                }

                var products = await db.Products.Join(db.Vendors, pro => pro.vendorId, ven => ven.Id, (pro, ven) => new
                {
                    pro,
                    ven

                }).Where(pro => pro.ven.city.ToLower() == city.ToLower() && pro.pro.status == "true").Select(pro =>
                new
                {
                    pro.pro.Id,
                    pro.pro.foodCatId,
                    pro.pro.calories,
                    pro.pro.cost,
                    pro.pro.coverImgUrl,
                    pro.pro.description,
                    pro.pro.minimumTime,
                    pro.pro.name,
                    pro.pro.vendorId,
                    pro.ven.city,
                    pro.ven.restaurantName,
                }).ToListAsync();

                var row = GetLists(city);
                var popularSearches = (from pro in db.Products
                                       where row.Contains(pro.Id) && pro.status == "true"
                                       select new
                                       {
                                           pro.Id,
                                           pro.calories,
                                           pro.cost,
                                           pro.coverImgUrl,
                                           pro.description,
                                           pro.foodCatId,
                                           pro.minimumTime,
                                           pro.name,
                                           pro.vendorId,
                                       }).AsEnumerable().OrderBy(pro => row.IndexOf(pro.Id)).ToList();
                return new { status = "success", mycategories, vendors, products, popularSearches };
            }
            catch (Exception)
            {

                throw;
            }
            
        }
       
        public async Task<object> Filter(ProductFilterViewModel vm) 
        {
            try
            {
                if ( vm.ids.Length != 0   && vm.from == null && vm.to == null)
                {
                    var products = await db.Products.Join(db.Vendors, pro => pro.vendorId, ven => ven.Id, (pro, ven) => new
                    {
                        pro,
                        ven

                    }).Select(pro =>
                    new {
                        pro.pro.Id,
                        pro.pro.foodCatId,
                        pro.pro.calories,
                        pro.pro.cost,
                        pro.pro.coverImgUrl,
                        pro.pro.description,
                        pro.pro.minimumTime,
                        pro.pro.name,
                        pro.pro.vendorId,
                        pro.ven.city,
                        pro.pro.status,

                    }).Where(pro => pro.city == vm.city && vm.ids.Contains(pro.foodCatId) && pro.status == "true").ToListAsync();
                    return new { status = "success", data = products };
                }
                else if (vm.ids.Length == 0 && vm.from != null && vm.to != null)
                {
                    var products = await db.Products.Join(db.Vendors, pro => pro.vendorId, ven => ven.Id, (pro, ven) => new
                    {
                        pro,
                        ven

                    }).Select(pro =>
                    new {
                        pro.pro.Id,
                        pro.pro.foodCatId,
                        pro.pro.calories,
                        pro.pro.cost,
                        pro.pro.coverImgUrl,
                        pro.pro.description,
                        pro.pro.minimumTime,
                        pro.pro.name,
                        pro.pro.vendorId,
                        pro.ven.city,
                        pro.pro.status,


                    }).Where(pro => pro.city == vm.city && pro.cost >= vm.from && pro.cost <= vm.to && pro.status == "true").ToListAsync();

                    return new { status = "success", data = products };

                }
                else if (vm.ids.Length != 0 && vm.from != null && vm.to != null)
                {
                    var products = await db.Products.Join(db.Vendors, pro => pro.vendorId, ven => ven.Id, (pro, ven) => new
                    {
                        pro,
                        ven

                    }).Select(pro =>
                    new {
                        pro.pro.Id,
                        pro.pro.foodCatId,
                        pro.pro.calories,
                        pro.pro.cost,
                        pro.pro.coverImgUrl,
                        pro.pro.description,
                        pro.pro.minimumTime,
                        pro.pro.name,
                        pro.pro.vendorId,
                        pro.ven.city,
                        pro.pro.status,

                    }).Where(pro => pro.city == vm.city && vm.ids.Contains(pro.foodCatId) && pro.cost >= vm.from && pro.cost <= vm.to && pro.status == "true").ToListAsync();

                    return new { status = "success", data = products };
                }
                else
                {
                    var products = await db.Products.Join(db.Vendors, pro => pro.vendorId, ven => ven.Id, (pro, ven) => new
                    {
                        pro,
                        ven

                    }).Select(pro=>new {
                        pro.pro.Id,
                        pro.pro.foodCatId,
                        pro.pro.calories,
                        pro.pro.cost,
                        pro.pro.coverImgUrl,
                        pro.pro.description,
                        pro.pro.minimumTime,
                        pro.pro.name,
                        pro.pro.vendorId,
                        pro.ven.city,
                        pro.pro.status,
                    }).Where(pro => pro.city == vm.city && pro.status == "true").ToListAsync();
                    return new { status = "success", data = products };
                }
            }
            catch (Exception)
            {

                throw;
            }
         
        }
        public async Task<object> Getall() 
        {
            var products = await db.Products.Join(db.FoodCategories, pro => pro.foodCatId, cat => cat.Id, (pro, cat) => new { pro, cat }).
                Join(db.Vendors, pro => pro.pro.vendorId, ven => ven.Id, (pro, ven) => new { pro.pro, ven ,pro.cat}).Select(pro => new
            {
                    pro.pro.Id,
                    pro.pro.name,
                    pro.pro.minimumTime,
                    pro.pro.calories,
                    pro.pro.cost,
                    pro.pro.coverImgUrl,
                    pro.pro.description,
                    pro.pro.foodCatId,
                    pro.pro.vendorId,
                    pro.pro.status,
                    category = pro.cat.name,
                    vendor = pro.ven.restaurantName



                }).ToListAsync();
         

            if (products != null)
            {
                return new { status = "success", data = products };
            }
            else
            {
                return new { status = "success", data = "Not Found" };

            }
        }
        public async Task<object> Status(long id, string status) 
        {
            var row = await db.Products.AsNoTracking().Where(pro => pro.Id == id).FirstOrDefaultAsync();
            if (row != null)
            {
                if (status == "true")
                {
                    Product pro = new Product();
                    pro.Id = row.Id;
                    pro.name = row.name;
                    pro.cost = row.cost;
                    pro.foodCatId = row.foodCatId;
                    pro.calories = row.calories;
                    pro.description = row.description;
                    pro.coverImgUrl = row.coverImgUrl;
                    pro.vendorId = row.vendorId;
                    pro.status = "true";
                    pro.minimumTime = row.minimumTime;
                    pro.keywords = row.keywords;

                    try
                    {
                        db.Entry(pro).State = EntityState.Modified;
                        int check = await db.SaveChangesAsync();
                        if (check > 0)
                        {
                            return  new { data = "updated", status = "success" };
                        }
                        else
                        {
                            return  new { data = "something went wrong", status = "error" };

                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
                else if (status == "false")
                {
                    Product pro = new Product();
                    pro.Id = row.Id;
                    pro.name = row.name;
                    pro.cost = row.cost;
                    pro.foodCatId = row.foodCatId;
                    pro.calories = row.calories;
                    pro.description = row.description;
                    pro.coverImgUrl = row.coverImgUrl;
                    pro.vendorId = row.vendorId;
                    pro.status = "false";
                    pro.minimumTime = row.minimumTime;
                    pro.keywords = row.keywords;
                    try
                    {
                        db.Entry(pro).State = EntityState.Modified;
                        int check = await db.SaveChangesAsync();
                        if (check > 0)
                        {
                            return  new { data = "updated", status = "success" };
                        }
                        else
                        {
                            return  new { data = "something went wrong", status = "error" };
                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
                else if (status == "suspended")
                {
                    Product pro = new Product();
                    pro.Id = row.Id;
                    pro.name = row.name;
                    pro.cost = row.cost;
                    pro.foodCatId = row.foodCatId;
                    pro.calories = row.calories;
                    pro.description = row.description;
                    pro.coverImgUrl = row.coverImgUrl;
                    pro.vendorId = row.vendorId;
                    pro.status = "suspended";
                    pro.minimumTime = row.minimumTime;
                    pro.keywords = row.keywords;

                    try
                    {
                        db.Entry(pro).State = EntityState.Modified;
                        int check = await db.SaveChangesAsync();
                        if (check > 0)
                        {
                            return new { data = "updated", status = "success" };
                        }
                        else
                        {
                            return  new { data = "something went wrong", status = "error" };
                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
                else
                {
                    return new { data = "something went wrong", status = "error" };
                }

            }
            else
            {
                return  new { data = "something went wrong", status = "error" };

            }
        }
    }
}
