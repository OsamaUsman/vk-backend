﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Enc;
using VirtualKitchenCore.Models.Tables;
using VirtualKitchenCore.Models.ViewModels;

namespace VirtualKitchenCore.Models.Repo
{
    public class VendorRepo : Repo<Vendor, VKDbContext>
    {
        VKDbContext db;
        public VendorRepo(VKDbContext db) : base(db)
        {
            this.db = db;
        }

        public async Task<object> Add(VendorViewModel vm) 
        {
            if (vm.user != null)
            {
                if (vm.user.userName == null || vm.user.userName == "" || vm.user.password == null || vm.user.password == "" || vm.user.role == null ||
                    vm.user.role == "" || vm.user.email == null || vm.user.email == "")
                {
                    return new { status = "error", data = "Bad Request." };
                }
                else 
                {
                    var enc = EncDec.Encrypt(vm.user.password);
                    vm.user.password = enc;
                    vm.user.role = "vendor";
                    vm.user.status = "pending";
                    db.Users.Add(vm.user);
                    int check = await db.SaveChangesAsync();
                    long userId = vm.user.Id;
                    if (check > 0)
                    {
                        var protype = string.Join(",", vm.vendor.specialties);
                        vm.vendor.specialties = protype;
                        vm.vendor.userId = userId;
                        db.Vendors.Add(vm.vendor);
                        int chec =  await db.SaveChangesAsync();
                        long vendorId = vm.vendor.Id;
                        if (chec > 0)
                        {
                            if (vm.emiratesids != null && vm.productsamples != null && vm.tradelicenses != null)
                            {
                                for (int i = 0; i < vm.emiratesids.Length; i++)
                                {
                                    VendorDoc doc = new VendorDoc();
                                    doc.type = "id";
                                    doc.document = vm.emiratesids[i];
                                    doc.vendorId = vendorId;

                                    db.VendorDocs.Add(doc);

                                }
                                try
                                {
                                   await db.SaveChangesAsync();

                                }
                                catch (Exception)
                                {

                                    throw;
                                }
                                for (int i = 0; i < vm.tradelicenses.Length; i++)
                                {
                                    VendorDoc doc = new VendorDoc();
                                    doc.type = "license";
                                    doc.document = vm.tradelicenses[i];
                                    doc.vendorId =vendorId;

                                    db.VendorDocs.Add(doc);

                                }
                                try
                                {
                                   await db.SaveChangesAsync();

                                }
                                catch (Exception)
                                {

                                    throw;
                                }
                                for (int i = 0; i < vm.productsamples.Length; i++)
                                {
                                    VendorDoc doc = new VendorDoc();
                                    doc.type = "sample";
                                    doc.document = vm.productsamples[i];
                                    doc.vendorId = vendorId;

                                    db.VendorDocs.Add(doc);

                                }
                                try
                                {
                                    int checkAll = await db.SaveChangesAsync();
                                    if (checkAll > 0)
                                    {
                                        if (vm.partners == null) 
                                        {
                                            return new { vendorId = vendorId, data = "Added", status = "success" };

                                        }
                                        else if ( vm.partners.ElementAt(0).name == ""  || vm.partners.ElementAt(0).emiratesId == "" || vm.partners.ElementAt(0).email == "")
                                        {
                                            return new { vendorId = vendorId, data = "Added", status = "success" };

                                        }
                                        else 
                                        {
                                          
                                            foreach (var item in vm.partners)
                                            {
                                                item.vendorId = vendorId;
                                            }
                                            await db.VendorPartners.AddRangeAsync(vm.partners);
                                            int ch =  await  db.SaveChangesAsync();
                                            if (ch > 0)
                                            {
                                                return new { vendorId = vendorId, data = "Added", status = "success" };

                                            }
                                            else 
                                            {
                                             return new { status = "error"  ,data = "something went wrong on adding vendor partners.", };

                                            }
                                        }
                                    }
                                    else 
                                    {
                                        return new { status = "error"  ,data = "something went wrong on adding vendor documents.", };
                                    }
                                }
                                catch (Exception)
                                {

                                    throw;
                                }
                            }


                            else
                            {
                                return new { data = "something went wrong", status = "error" };
                            }
                        }
                        else 
                        {
                         return new { status = "error", data = "something went wrong on adding vendor." };
                        }
                    }
                    else 
                    {
                      return new { status = "error", data = "something went wrong on adding user." };

                    }
                }
            }
            else
            {
                return new { status = "error", data = "Bad Request." };
            }
        }

        public async Task<object> GetAllVendors()
        {
            try
            {
                var vendor = await db.Vendors.Join(db.Users, ven => ven.userId, us => us.Id, (ven, us) => new { ven, us }).Select(ven => new
                {
                    ven.ven.Id,
                    ven.ven.address,
                    ven.ven.restaurantName,
                    ven.ven.userId,
                    ven.ven.city,
                    ven.ven.contactNo,
                    ven.ven.cover,
                    ven.ven.aboutUs,
                    ven.ven.latitude,
                    ven.ven.logo,
                    ven.ven.longitude,
                    ven.ven.specialties,
                    ven.ven.url,
                    ven.ven.landlineNo,
                    ven.ven.licenseSource,
                    ven.ven.tradeLicenseNo,
                    ven.ven.tradeLicenseExpiry,
                    ven.us.status,
                    ven.us.userName,
                    ven.us.email
                }).ToListAsync();

                
                return new { status = "success", data=vendor};
            }
            catch (Exception)
            {

                throw;
            }

        }

        public async Task<object> VendorDetails(long id) 
        {
            try
            {
                var vendor = await db.Vendors.Join(db.Users, ven => ven.userId, us => us.Id, (ven, us) => new { ven, us }).Select(ven => new
                {
                    ven.ven.Id,
                    ven.ven.address,
                    ven.ven.restaurantName,
                    ven.ven.userId,
                    ven.ven.city,
                    ven.ven.contactNo,
                    ven.ven.cover,
                    ven.ven.aboutUs,
                    ven.ven.latitude,
                    ven.ven.logo,
                    ven.ven.longitude,
                    ven.ven.specialties,
                    ven.ven.url,
                    ven.ven.landlineNo,
                    ven.ven.licenseSource,
                    ven.ven.tradeLicenseNo,
                    ven.ven.tradeLicenseExpiry,
                    ven.us.status,
                    ven.us.userName,
                    ven.us.email
                }).Where(ven => ven.Id == id).FirstOrDefaultAsync();

                var partners = await db.VendorPartners.Where(vendorp => vendorp.vendorId == id).ToListAsync();
                var docs = await db.VendorDocs.Where(vendorc => vendorc.vendorId == id).ToListAsync();

                return new { status = "success", vendor, partners, docs };
            }
            catch (Exception)
            {

                throw;
            }
            
        }
        public async Task<object> Status(long id, string status) 
        {
            var row = await  db.Users.AsNoTracking().Where(ven => ven.Id == id).FirstOrDefaultAsync();
            if (row != null)
            {
                if (status == "true")
                {
                    User us = new User();
                    us.Id = row.Id;
                    us.firstName = row.firstName;
                    us.userName = row.userName;
                    us.lastName = row.lastName;
                    us.password = row.password;
                    us.verificationCode = row.verificationCode;
                    us.email = row.email;
                    us.mobileno = row.mobileno;
                    us.role = row.role;
                    us.suspendedTill = row.suspendedTill;
                    us.status = "approved";
                    try
                    {
                        db.Entry(us).State = EntityState.Modified;
                        int check = await db.SaveChangesAsync();
                        if (check > 0)
                        {
                            return new { data = "updated", status = "success" };
                        }
                        else
                        {
                            return new { data = "something went wrong", status = "error" };

                        }
                    }
                    catch (Exception ex)
                    {

                        return ex ;
                    }
                }
                else if (status == "false")
                {
                    User us = new User();
                    us.Id = row.Id;
                    us.userName = row.userName;
                    us.firstName = row.firstName;
                    us.lastName = row.lastName;
                    us.password = row.password;
                    us.verificationCode = row.verificationCode;
                    us.email = row.email;
                    us.mobileno = row.mobileno;
                    us.role = row.role;
                    us.suspendedTill = row.suspendedTill;
                    us.status = "declined";

                    try
                    {
                        db.Entry(us).State = EntityState.Modified;
                        int check = db.SaveChanges();
                        if (check > 0)
                        {
                            return  new { data = "updated", status = "success" };
                        }
                        else
                        {
                            return  new { data = "something went wrong", status = "error" };
                        }
                    }
                    catch (Exception ex)
                    {

                        return  ex;
                    }
                }
                else if (status == "suspended")
                {
                    User us = new User();
                    us.Id = row.Id;
                    us.userName = row.userName;
                    us.firstName = row.firstName;
                    us.lastName = row.lastName;
                    us.password = row.password;
                    us.verificationCode = row.verificationCode;
                    us.email = row.email;
                    us.mobileno = row.mobileno;
                    us.role = row.role;
                    us.suspendedTill = row.suspendedTill;
                    us.status = "suspended";

                    try
                    {
                        db.Entry(us).State = EntityState.Modified;
                        int check = db.SaveChanges();
                        if (check > 0)
                        {
                            return  new { data = "updated", status = "success" };
                        }
                        else
                        {
                            return  new { data = "something went wrong", status = "error" };
                        }
                    }
                    catch (Exception ex)
                    {

                        return ex;
                    }
                }
                else if (status == "active")
                {
                    User us = new User();
                    us.Id = row.Id;
                    us.userName = row.userName;
                    us.firstName = row.firstName;
                    us.lastName = row.lastName;
                    us.password = row.password;
                    us.verificationCode = row.verificationCode;
                    us.email = row.email;
                    us.mobileno = row.mobileno;
                    us.role = row.role;
                    us.suspendedTill = row.suspendedTill;
                    us.status = "active";

                    try
                    {
                        db.Entry(us).State = EntityState.Modified;
                        int check = db.SaveChanges();
                        if (check > 0)
                        {
                            return  new { data = "updated", status = "success" };
                        }
                        else
                        {
                            return  new { data = "something went wrong", status = "error" };
                        }
                    }
                    catch (Exception ex)
                    {

                        return  ex;
                    }
                }
                else
                {
                    return new { data = "something went wrong", status = "error" };
                }

            }
            else
            {
                return new { data = "something went wrong", status = "error" };
            }
        }
    }
}
