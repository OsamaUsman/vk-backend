﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;
using VirtualKitchenCore.Models.ViewModels;

namespace VirtualKitchenCore.Models.Repo
{
    public class IngredientRepo : Repo<Ingredient, VKDbContext>
    {
        VKDbContext db;
        public IngredientRepo(VKDbContext db) : base(db)
        {
            this.db = db;
        }
        public async Task<object> GetIngredient(long id)
        {
            try
            {
                var row = await db.Ingredients.Join(db.KitchenCategories , ing =>ing.kitchenCategoryId , kit=>kit.Id , (ing,kit) =>new {ing,kit }).Where(ing => ing.kit.vendorId == id).
                Select(ing=> new 
                {
                    ing.ing.Id,
                    ing.ing.imageUrl,
                    ing.ing.kitchenCategoryId,
                    ing.ing.standardQty,
                    ing.ing.name,
                    ing.ing.units,
                    ing.ing.variants,
                    ing.ing.price,
                    Kitchen = ing.kit.name,

                }).ToListAsync();
                if (row != null)
                {
                    return new { status = "success", data = row };
                }
                else
                {
                    return new { status = "success", data = "Not Found" };
                }

            }
            catch (Exception)
            {

                throw;
            }

        }

        public async Task<object> GetIngredientByKitchen(long id)
        {
            try
            {
                var row = await db.Ingredients.Join(db.KitchenCategories, ing => ing.kitchenCategoryId, kit => kit.Id, (ing, kit) => new { ing, kit }).Where(ing => ing.ing.kitchenCategoryId == id).
                Select(ing => new
                {
                    ing.ing.Id,
                    ing.ing.imageUrl,
                    ing.ing.kitchenCategoryId,
                    ing.ing.standardQty,
                    ing.ing.name,
                    ing.ing.units,
                    ing.ing.variants,
                    ing.ing.price,
                    Kitchen = ing.kit.name,

                }).ToListAsync();
                if (row != null)
                {
                    return new { status = "success", data = row };
                }
                else
                {
                    return new { status = "success", data = "Not Found" };
                }

            }
            catch (Exception)
            {

                throw;
            }

        }
        public async Task<object> GetIngredientbyK(long id) 
        {
            var row = await db.Ingredients.Join(db.KitchenCategories, ing => ing.kitchenCategoryId, kit => kit.Id, (ing, kit) => new { ing, kit }).Where(ing => ing.ing.kitchenCategoryId == id).
                Select(ing => new
                {
                    ing.ing.Id,
                    ing.ing.imageUrl,
                    ing.ing.kitchenCategoryId,
                    ing.ing.standardQty,
                    ing.ing.name,
                    ing.ing.units,
                    ing.ing.variants,
                    ing.ing.price,
                    Kitchen = ing.kit.name,

                }).ToListAsync();

            List<IngredientsViewModel> mylist = new List<IngredientsViewModel>();
            foreach (var item in row)
            {
                IngredientsViewModel vm = new IngredientsViewModel();
                vm.Id = item.Id;
                vm.imageUrl = item.imageUrl;
                vm.kitchenCategoryId = item.kitchenCategoryId;
                vm.name = item.name;
                vm.standardQty = item.standardQty;
                vm.price = item.price;
                vm.variants = (item.variants == null) ? null : item.variants.Split(',');
                vm.units = (item.units == null) ? null : item.units.Split(',');
                mylist.Add(vm);
            }

            if (mylist != null)
            {
                return new { status = "success", data = mylist };
            }
            else
            {
                return new { status = "success", data = mylist };
            }
        }
    }
}
