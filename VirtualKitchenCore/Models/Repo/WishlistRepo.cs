﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.Repo
{
    public class WishlistRepo : Repo<Wishlist, VKDbContext>
    {
        VKDbContext db;
        public WishlistRepo(VKDbContext db) : base(db)
        {
            this.db = db;
        }
    }
}
