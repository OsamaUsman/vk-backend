﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.Repo
{
    public class VendorPartnerRepo : Repo<VendorPartner,VKDbContext>
    {
        VKDbContext db;
        public VendorPartnerRepo(VKDbContext db) :base (db)
        {
            this.db = db;
        }
    }
}
