﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;
using VirtualKitchenCore.Models.ViewModels;

namespace VirtualKitchenCore.Models.Repo
{
    public class AddressRepo : Repo<Address,VKDbContext>
    {
        VKDbContext db;
        public AddressRepo(VKDbContext db) : base(db) 
        {
            this.db = db;
        }
        // var query =  db.Addresses.Join(
        //    db.Users,
        //    ad => ad.userId,
        //    us => us.Id,
        //    (ad, us) => new
        //    {
        //        ad.Id,
        //        ad.title,
        //        ad.date,
        //        ad.deviceId,
        //        ad.latitude,
        //        ad.longitude,
        //        ad.userId,
        //        ad.mapAddress,
        //        ad.mobilePhone,
        //        ad.name,
        //        ad.addressOne,
        //        ad.city
        //    }
        //).Where(a => a.Id == vm.).ToList(); 
        //).Where(a => a.Id == vm.).ToList(); 
        public async Task<object> Get(AddressViewModel vm)
        {
            try
            {
                var query = await db.Addresses.Where(ad => ad.userId == vm.userid || ad.deviceId == vm.DeviceId).ToListAsync();
                if (query == null)
                {
                    return new { status = "success", data = "Not Found"};
                }
                else 
                {
                    return new { status = "success", data = query };
                }
            }
            catch (Exception)
            {

                throw;
            }
     
        }

        public async Task<object> Update(long id, Address address)
        {

            if (address != null)
            {
                var row = await db.Orders.Where(o => o.addressId == id).FirstOrDefaultAsync();
                if (row == null)
                {
                    db.Entry(address).State = EntityState.Modified;

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        throw;
                    }

                    var addresses = await db.Addresses.Where(o => o.userId == address.userId).ToListAsync();

                    return new { status = "success", data = addresses };

                }

                else
                {
                    var check = row.status.ToLower();
                    if (check == "completed")
                    {
                        db.Entry(address).State = EntityState.Modified;

                        try
                        {
                            db.SaveChanges();
                        }
                        catch (DbUpdateConcurrencyException)
                        {

                        }
                        var addresses = await db.Addresses.Where(o => o.userId == address.userId).ToListAsync();

                        return new { status = "success", data = addresses };
                        //var parent = db.addresses.Include(p => p.orderDetails)
                        //.SingleOrDefault(p => p.addressId == id);

                        //foreach (var child in parent.orderDetails.ToList())
                        //    db.orderDetails.Remove(child);
                    }
                    else
                    {
                        return new { status = "success", data = "Order in process." };
                    }
                }




            }
            else
            {
                return new {status="error",data="something went wrong." };
            }

        }

        public async Task<object> Remove(long id)
        {
            // var myaddress = await db.Addresses.Where(ad=> ad.Id == id).ToListAsync();
             Address address = await db.Addresses.FindAsync(id);
            // var myrow = await db.Addresses.Where(add=> add.Id == id).ToListAsync();


            if (address != null)
            {
                var row = await db.Orders.Where(o => o.addressId == id).FirstOrDefaultAsync();
                
                if (row == null)
                {
                    db.Addresses.Remove(address);
                    try
                    {
                     await db.SaveChangesAsync();
                    }
                    catch (Exception)
                    {
                        throw;

                    }
                    var myrow = await db.Addresses.Where(add => add.userId == address.userId).ToListAsync();

                    return new { status = "success", data = myrow  };

                }
                else
                {

                    var check = row.status.ToLower();
                    if (check == "completed")
                    {
                        db.Addresses.Remove(address);
                        try
                        {
                           await db.SaveChangesAsync();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                        var myrow = await db.Addresses.Where(add => add.userId == address.userId).ToListAsync();

                        return new { status = "success", data = myrow };


                    }
                    else
                    {
                        return new { status = "success", data = "Order in process." }; 
                    }
                }

             
            }
            else
            {
                return new { status = "error", data = "something went wrong." };

            }

        }
        public async Task<object> AddAddress(Address address) 
        {
            try
            {
                await db.Addresses.AddAsync(address);
                int check = await db.SaveChangesAsync();
                if (check > 0)
                {
                    var addresses = await db.Addresses.Where(o => o.userId == address.userId).ToListAsync();

                    return new { status = "success", data = addresses };

                }
                else 
                {
                  return new { status = "error", data = "something went wrong." };

                }
            }
            catch (Exception)
            {

                throw;
            }
           
            
        }
    }
}
