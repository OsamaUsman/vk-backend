﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.Repo
{
    public class ProductImgRepo : Repo<ProductImage,VKDbContext>
    {
        private readonly VKDbContext db;
        public ProductImgRepo(VKDbContext db) :base(db)
        {
            this.db = db;
        }
    }
}
