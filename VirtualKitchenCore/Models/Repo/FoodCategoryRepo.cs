﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualKitchenCore.Models.Tables;

namespace VirtualKitchenCore.Models.Repo
{
    public class FoodCategoryRepo : Repo<FoodCategory,VKDbContext>
    {
        VKDbContext db;
        public FoodCategoryRepo(VKDbContext db) :base(db)
        {
            this.db = db;
        }
        public async Task<object> Remove(long id) 
        {
            var product = db.Products.Where(product => product.foodCatId == id).FirstOrDefault();
            if (product != null)
            {
                return new { status = "exist", data = "this category has product added." };
            }
            else 
            {
                var category = db.FoodCategories.Where(category => category.Id == id).FirstOrDefault();

                db.FoodCategories.Remove(category);
                int check = await db.SaveChangesAsync();
                if (check > 0)
                {
                    return new { status = "success", data = "deleted" };
                }
                else 
                {
                    return new { status = "error", data = "something went wrong." };

                }
            }
        }
    }
}
