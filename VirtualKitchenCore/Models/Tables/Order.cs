﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class Order
    {
        [Key]
        public long Id { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string modeOfpayment { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal bill { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal vat { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal deliveryCharges { get; set; }


        [Column(TypeName = "decimal(10,2)")]
        public decimal totalBill { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal discount { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal netBill { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string url { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string transactionId { get; set; }
        
        public DateTime date { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string status { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal paidAmount { get; set; }
        
        public long deliveryModeId { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string note { get; set; }
        
        [Display(Name = "User")]
        public long? userId { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("userId")]
        public virtual User User { get; set; }

        [Display(Name = "Vendor")]
        public long? vendorId { get; set; }


        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("vendorId")]
        public virtual Vendor Vendor { get; set; }
        [Column(TypeName = "nvarchar(150)")]
        public string latitude { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string longitude { get; set; }


        [Column(TypeName = "nvarchar(150)")]
        public string addressTitle { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string name { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string address { get; set; }
        public long? addressId { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string contactNo { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string streetNo { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Chat> Chats { get; set; }
       






    }
}
