﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class Ingredient
    {
        [Key]
        public long Id { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string name { get; set; }
        
        [Column(TypeName = "nvarchar(1000)")]
        public string imageUrl { get; set; }

        [Column(TypeName = "nvarchar(1500)")]
        public string variants { get; set; }

        [Column(TypeName = "nvarchar(1500)")]
        public string units { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal price { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string standardQty { get; set; }

        [Display(Name ="KitchenCategory")]
        public long kitchenCategoryId { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("kitchenCategoryId")]
        public virtual KitchenCategory KitchenCategory { get; set; }

    }
}
