﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class User
    {
        [Key]
        public long Id { get; set; }
            
        [Column(TypeName = "nvarchar(50)")]
        public string userName { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string firstName { get; set; }
        
        [Column(TypeName = "nvarchar(50)")]
        public string lastName { get; set; }
        
        [Column(TypeName = "nvarchar(50)")]
        public string email { get; set; }
        
        [Column(TypeName = "nvarchar(200)")]
        public string password { get; set; }
        
        [Column(TypeName = "nvarchar(50)")]
        public string mobileno { get; set; }
        
        [Column(TypeName = "nvarchar(50)")]
        public string role { get; set; }
        
        [Column(TypeName = "nvarchar(50)")]
        public string status { get; set; }
        public DateTime suspendedTill { get; set; }
        public long verificationCode { get; set; }

        public virtual ICollection<Cart> Carts  { get; set; }
        public virtual ICollection<Chat> Chats  { get; set; }
        public virtual ICollection<Vendor> Vendors  { get; set; }
        public virtual ICollection<Address> Addresses  { get; set; }
        public virtual ICollection<Wishlist> Wishlists  { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

    }
}
