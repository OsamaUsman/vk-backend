﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class Address
    {
        [Key]
        public long Id { get; set; }
        
        [Column(TypeName ="nvarchar(150)")]
        public string title { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string mapAddress { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string latitude { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string longitude  { get; set; }

        public DateTime date { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string deviceId { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string addressOne { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string city { get; set; }


        [Display(Name = "User")]
        public long? userId { get; set; }

        [ForeignKey("userId")]
        public virtual User User { get; set; }

    }
}
