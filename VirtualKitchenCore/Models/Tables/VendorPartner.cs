﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class VendorPartner
    {
        [Key]
        public long Id { get; set; }
        
        [Column(TypeName="nvarchar(250)")]
        public string name { get; set; }

        [Column(TypeName = "nvarchar(250)")]
        public string email { get; set; }
        
        [Column(TypeName = "nvarchar(250)")]
        public string emiratesId { get; set; }

        [Display(Name = "Vendor")]
        public long? vendorId { get; set; }

        [ForeignKey("vendorId")]
        public virtual Vendor Vendor { get; set; }

    }
}
