﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class Vendor
    {
        [Key]
        public long Id { get; set; }

        [Display(Name = "User")]
        public long? userId { get; set; }

        [ForeignKey("userId")]
        public virtual User User { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string restaurantName { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string email { get; set; }

        [Column(TypeName = "nvarchar(1000)")]
        public string aboutUs { get; set; }


        [Column(TypeName = "nvarchar(50)")]
        public string city { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string url { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string country { get; set; }


        [Column(TypeName = "nvarchar(250)")]
        public string tradeLicenseNo { get; set; }

        [Column(TypeName = "nvarchar(250)")]
        public string tradeLicenseExpiry { get; set; }

        [Column(TypeName = "nvarchar(250)")]
        public string licenseSource { get; set; }

        [Column(TypeName = "nvarchar(1000)")]
        public string specialties { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string status { get; set; }


        [Column(TypeName = "nvarchar(500)")]
        public string cover { get; set; }
        
        [Column(TypeName = "nvarchar(500)")]
        public string logo { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string mapAddress { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string latitude { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string longitude { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string contactNo { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string landlineNo { get; set; }

        [Column(TypeName = "nvarchar(200)")]
        public string address { get; set; }


   

        public virtual ICollection<Chat> Chats { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<VendorDoc> VendorDocs { get; set; }
        public virtual ICollection<VendorPartner> VendorPartners { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<KitchenCategory> KitchenCategories { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }


    }
}
