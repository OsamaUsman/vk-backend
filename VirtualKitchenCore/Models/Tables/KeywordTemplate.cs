﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class KeywordTemplate
    {
        [Key]
        public long Id { get; set; }

        [Column(TypeName = "nvarchar(250)")]
        public string title { get; set; }
        
        [Column(TypeName = "nvarchar(1550)")]
        public string keywords { get; set; }


    }
}
