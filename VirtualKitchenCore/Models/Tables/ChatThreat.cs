﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class ChatThreat
    {
        [Key]
        public long Id { get; set; }
        public long userId { get; set; }
    
        public long orderId { get; set; }
     
        public long vendorId { get; set; }

        public DateTime date { get; set; }
    }
}
