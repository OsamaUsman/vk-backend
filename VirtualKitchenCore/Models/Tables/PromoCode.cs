﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class PromoCode
    {
        [Key]
        public long Id { get; set; }
        
        [Column(TypeName="nvarchar(250)")]
        public string code { get; set; }

        [Column(TypeName="decimal(10,2)")]
        public decimal discountPercentage { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal maximumLimit { get; set; }

        public DateTime validity { get; set; }
    }
}
