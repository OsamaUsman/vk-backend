﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class Wishlist
    {
        [Key]
        public long Id { get; set; }

        [Display(Name = "User")]
        public long? userId { get; set; }

        [ForeignKey("userId")]
        public virtual User User { get; set; }

        [Display(Name = "Product")]
        public long productId { get; set; }

        [ForeignKey("productId")]
        public virtual Product Product { get; set; }

        [Column(TypeName = "nvarchar(550)")]
        public string deviceId { get; set; }
    }
}
