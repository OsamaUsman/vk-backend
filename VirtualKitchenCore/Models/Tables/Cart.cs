﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class Cart
    {
        [Key]
        public long Id { get; set; }

        [Display(Name = "User")]
        public long userId { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("userId")]
        public virtual User User  { get; set; }
        
        [Display(Name ="Product")]
        public long? productId { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("productId")]
        public virtual Product Product { get; set; }

        [Column(TypeName ="nvarchar(1500)")]
        public string variants { get; set; }

        [Column(TypeName = "nvarchar(1500)")]
        public string addOns { get; set; } 
        
        [Column(TypeName = "nvarchar(150)")]
        public string type { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string quantity { get; set; } 
        
        [Column(TypeName = "nvarchar(1500)")]
        public string ingredients { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal amount { get; set; } 
        public long? kitchenId { get; set; } 
        public long? vendorId { get; set; } 
        public string addOnsIds { get; set; } 
        public long?  variantId { get; set; } 
        
    }
}
