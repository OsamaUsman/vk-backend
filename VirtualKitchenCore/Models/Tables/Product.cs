﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class Product
    {
        [Key]
        public long Id { get; set; }

        [Display(Name = "Vendor")]
        public long? vendorId { get; set; }


        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("vendorId")]
        public virtual Vendor Vendor { get; set; }

        [Column(TypeName="nvarchar(250)")]
        public string name { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string description { get; set; }
        
        [Column(TypeName = "nvarchar(1000)")]
        public string coverImgUrl { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal? cost { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string minimumTime { get; set; }


        [Column(TypeName = "nvarchar(250)")]
        public string calories { get; set; }

        [Column(TypeName = "nvarchar(1500)")]
        public string variant { get; set; }
        
        [Column(TypeName = "nvarchar(150)")]
        public string status { get; set; }

        [Column(TypeName = "nvarchar(1550)")]
        public string keywords { get; set; }

        [Display(Name = "FoodCategory")]
        public long? foodCatId { get; set; }

        [ForeignKey("foodCatId")]
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual FoodCategory FoodCategory { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Cart> Carts { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Wishlist> Wishlists { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<ProductImage> ProductImages { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<AddOn> AddOns { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Variant> Variants { get; set; }
    }
}
