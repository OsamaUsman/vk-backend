﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class ProductImage
    {
        [Key]
        public long Id { get; set; }
        
        [Column (TypeName= "nvarchar(1000)")]
        public string imageUrl { get; set; }

        [Display(Name = "Product")]  
        public long productId { get; set; }

        [ForeignKey("productId")]
        public virtual Product Product { get; set; }
    }
}
