﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class VKDbContext : DbContext
    {
        public VKDbContext(DbContextOptions options): base(options)
        {
          
        }
        public DbSet<FoodCategory> FoodCategories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        
        public DbSet<Cart> Carts { get; set; }
        public DbSet<KitchenCategory> KitchenCategories { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Address> Addresses { get; set; }
        
        public DbSet<AddOn> AddOns { get; set; }
        public DbSet<Variant> Variants { get; set; }



        public DbSet<Chat> Chats { get; set; }
        public DbSet<ChatThreat> ChatThreats { get; set; }
        public DbSet<Wishlist> Wishlists { get; set; }
        public DbSet<KeywordTemplate> KeywordTemplates { get; set; }
        public DbSet<VendorPartner> VendorPartners { get; set; }
        public DbSet<VendorDoc> VendorDocs { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }


        public DbSet<Order> Orders { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
    }
}
