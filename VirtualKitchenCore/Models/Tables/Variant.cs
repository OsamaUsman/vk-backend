﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class Variant
    {
        [Key]
        public long Id { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string name { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal addedPrice { get; set; }
       
        [Display(Name = "Product")]
        public long? productId { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("productId")]
        public virtual Product Product { get; set; }
    }
}
