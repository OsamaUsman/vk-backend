﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class OrderDetail
    {
        [Key]
        public long Id { get; set; }
        
        [Display(Name = "Order")]
        public long orderId { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("orderId")]
        public virtual Order Order { get; set; }

        [Display(Name = "Vendor")]
        public long vendorId { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("vendorId")]
        public virtual Vendor Vendor { get; set; }

        [Display(Name = "Product")]
        public long? productId { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("productId")]
        public virtual Product Product { get; set; }

        public DateTime date { get; set; }
        
        [Column(TypeName ="nvarchar(150)")]
        public string status { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal cost { get; set; }
      
        [Column(TypeName = "nvarchar(150)")]
        public string productName { get; set; }

        [Column(TypeName = "nvarchar(2000)")]
        public string variants { get; set; }
        
        [Column(TypeName = "nvarchar(2000)")]
        public string ingredients { get; set; }

        [Column(TypeName = "nvarchar(2000)")]
        public string addOns { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string quantity { get; set; }  
        
        [Column(TypeName = "nvarchar(150)")]
        public string type { get; set; }
       
        [Column(TypeName = "nvarchar(1550)")]
        public string addOnsIds { get; set; }
        public long? kitchenId { get; set; }

        public long? variantId { get; set; }
        [NotMapped]
        public string[] addOnsList { get; set; }
        [NotMapped]
        public string productImage { get; set; }
        [NotMapped]
        public string[] ingredientsList { get; set; }
    }
}
