﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class Chat
    {
        [Key]
        public long Id { get; set; }

        [Display(Name = "User")]
        public long userId { get; set; }

        [ForeignKey("userId")]
        public virtual User User { get; set; }

        [Display(Name = "Order")]
        public long orderId { get; set; }

        [ForeignKey("orderId")]
        public virtual Order Order { get; set; }

        [Display(Name = "Vendor")]
        public long vendorId { get; set; }

        [ForeignKey("vendorId")]
        public virtual Vendor Vendor { get; set; }

        [Column(TypeName ="nvarchar(3000)")]
        public  string message { get; set; }

        public bool sentBy { get; set; }
        public bool type { get; set; }
        public DateTime date { get; set; }
       
        [Column(TypeName ="nvarchar(150)")]
        public string status { get; set; }

    }
}
