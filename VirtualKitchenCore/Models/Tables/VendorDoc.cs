﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class VendorDoc
    {
        [Key]
        public long Id { get; set; }

        [Display(Name = "Vendor")]
        public long vendorId { get; set; }

        [ForeignKey("vendorId")]
        public virtual Vendor Vendor { get; set; }

        [Column(TypeName ="nvarchar(1500)")]
        public string document { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string type { get; set; }
    }
}
