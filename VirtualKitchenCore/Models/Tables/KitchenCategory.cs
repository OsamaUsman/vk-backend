﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class KitchenCategory
    {
        [Key]
        public long Id { get; set; }

        [Column(TypeName="nvarchar(150)")]
        public  string name { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string status { get; set; }
       
        [Column(TypeName = "nvarchar(1500)")]
        public string imageUrl { get; set; }

        [Display(Name ="Vendor")]
        public long vendorId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("vendorId")]
        public virtual Vendor Vendor { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Ingredient> Ingredients { get; set; }

    }
}
