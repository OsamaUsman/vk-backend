﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VirtualKitchenCore.Models.Tables
{
    public class FoodCategory
    {
        [Key]
        public long Id { get; set; }
        
        [Column(TypeName ="nvarchar(50)")]
        public string name { get; set; }
        
        public int sequence { get; set; }

        [Column(TypeName = "nvarchar(1000)")]
        public string imageUrl { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Product> Products { get; set; }
    }
}
